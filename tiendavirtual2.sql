-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 17-07-2020 a las 11:26:16
-- Versión del servidor: 10.4.13-MariaDB
-- Versión de PHP: 7.2.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `tiendavirtual`
--

DELIMITER $$
--
-- Procedimientos
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `prActualizarClienteporEmpleado` (IN `idCliente` INT, IN `nombrescompletos` VARCHAR(250), IN `correo` VARCHAR(250), IN `apellidos` VARCHAR(250), IN `direccion` VARCHAR(250), IN `tipodocumento` VARCHAR(1), IN `tipocliente` VARCHAR(45), IN `numerodocumento` VARCHAR(40), IN `distrito` VARCHAR(100), IN `numerocel` VARCHAR(25), IN `nombrempresa` VARCHAR(200), IN `idempleado` INT, IN `referencia` VARCHAR(500), IN `estadusuario` TINYINT)  BEGIN
    declare auxtipocliente  VARCHAR(250);
    declare auxidusuario int;
    declare auxidusuariocorreo int;
	declare auxidcliente int;
    declare auxclientedocumento int;
    SET auxclientedocumento=null;
    SET auxidcliente= null;
    SET auxtipocliente= "";
    SET auxidusuario  = null;
    set auxidusuariocorreo=null;
    
    select clienteusuario.fmidusuario into auxidusuario from fmusuario as usuario 
    inner join fmcliente_usuario as clienteusuario on clienteusuario.fmidusuario=usuario.fmidusuario
    where clienteusuario.fmidcliente=idCliente;
    if auxidusuario is null then
		select false as estado,'NO existe cliente con ese id.' as mensaje;
    else
        select usuario.fmidusuario into auxidusuariocorreo from fmusuario as usuario where usuario.fmcorreo=correo; 
		if auxidusuariocorreo is null or auxidusuariocorreo=auxidusuario then
         select cliente.fmidcliente into auxclientedocumento from fmcliente as cliente where cliente.fmidcliente=idcliente;
         if auxclientedocumento is null or auxclientedocumento=idcliente then
           update fmcliente
			  set 
              fmidempleado=idempleado,
			  fmnombrecompletos=nombrescompletos,
			  fmapellidos=apellidos,
			  fmultimafechamodificacion=current_timestamp(),
			  fmdireccion=direccion,
			  fmtipodocumento=tipodocumento,
			  fmtipocliente=tipocliente,
			  fmnumerodocumento=numerodocumento,
			  fmdistrito=distrito,
			  fmnumerocel=numerocel,
			  fmnombreempresa=nombrempresa,
			  fmreferencia=referencia
              where fmidcliente=idCliente;
          update fmusuario 
			  set 
			  fmcorreo=correo ,
              fmestado=coalesce(estadusuario,fmestado),
			  fmultimafechamodificacion=current_timestamp()
			  where fmidusuario=auxidusuario; 
		  select  true estado,'El usuario se actualizo con exito.' AS mensaje;
          else
			select false as estado,'el numero de documento pertenece a otro cliente' as mensaje;
         end if;
         
  
        else
          select false estado,'El correo ya esta en uso por otro usuario.'  mensaje;
        end if;
    end if;

   
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `prActualizarpedido` (IN `estadopedido` VARCHAR(15), IN `idpedido` INT, IN `idempleadoUltimoModificacion` INT)  BEGIN
	declare auxidpedido int;
    set auxidpedido=0;
	select count(*) into auxidpedido from fmpedido as pedido where pedido.fmidpedido=idpedido;
    if auxidpedido>0 then
		
		update fmpedido 
        set fmestado=estadopedido, 
        fmidempleadoUltimoModificacion=idempleadoUltimoModificacion,
        fmultimafechamodificacion=current_timestamp() 
        where fmidpedido=idpedido;
        select true as estado ,'El pedido se actualizo con exito.' as mensaje;
    else
		select false as estado ,'El pedido no existe.' as mensaje;
    end if;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `prconsultardetalleproducto` (IN `idproducto` INT)  BEGIN
    SELECT producto.fmdetalles as detalleproducto
    FROM fmproducto as producto where producto.fmidproducto=coalesce(idproducto,producto.fmidproducto) and producto.fmestado=1;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `prconsultarproducto` (IN `idcategoria` INT, IN `idproducto` INT, IN `novedad` INT)  BEGIN
    SELECT producto.fmidproducto as id,producto.fmnombreproducto as nombreproducto,producto.fmdescripcion as descripcion,
    producto.fmidcategoria as idcategoria,producto.fmnovedad as novedad,producto.fmprecioventa as precioventa,producto.fmestado as estado,
    producto.fmdetalles as detalles,producto.fmfechacreacion as fechacreacion,producto.fmultimafechamodificacion as fechaultimamodificacion,producto.fmidempleado as idempleado,
     empleado.fmnombres as nombreultimoempleado,empleado.fmapellidopaterno as apellidopaternoultimoempleado,empleado.fmapellidomaterno  apellidomaternoultimoempleado,producto.fmimg as img
    FROM fmproducto as producto
    inner join fmempleado as empleado on empleado.fmidempleado=producto.fmidempleado
    where  producto.fmestado=true and producto.fmidproducto=coalesce(idproducto,producto.fmidproducto) 
    and  producto.fmidcategoria=coalesce(idcategoria,producto.fmidcategoria) and producto.fmnovedad=coalesce(novedad,producto.fmnovedad)
order by producto.fmidcategoria asc ;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `prIniciarSession` (`correo` VARCHAR(250), `contrasenia` LONGTEXT)  BEGIN
    declare validacredenciales  int;
    declare tipousuario  varchar(100);
    set validacredenciales:=0;
	select count(*) into validacredenciales
	from fmusuario as usuario  
	where usuario.fmcorreo=correo and  usuario.fmcontrasenia=md5(contrasenia);
    if validacredenciales>0 then
    select usuario.fmtipousuario into tipousuario
	from fmusuario as usuario  
	INNER JOIN fmcliente_usuario as eee on eee.fmidusuario=usuario.fmidusuario
	INNER JOIN fmcliente as cliente on cliente.fmidcliente=eee.fmidcliente
	where usuario.fmcorreo=correo and  usuario.fmcontrasenia=md5(contrasenia);
    if tipousuario='CLIENTE' then
      select usuario.fmtipousuario AS tipousuario,cliente.fmidcliente as idcliente,cliente.fmnombrecompletos as nombrecompletos,cliente.fmapellidos as apellidos,
		usuario.fmidusuario as idusuario,usuario.fmcorreo as correo,true as estadoflag,'credenciales correcta' as mensaje
		from fmusuario as usuario  
		INNER JOIN fmcliente_usuario as eee on eee.fmidusuario=usuario.fmidusuario
		INNER JOIN fmcliente as cliente on cliente.fmidcliente=eee.fmidcliente
		where usuario.fmcorreo=correo and  usuario.fmcontrasenia=md5(contrasenia);
    else
		select usuario.fmtipousuario AS tipousuario, empleado.fmidempleado as idempleado,empleado.fmdocumentoidentidad as documentoidentidad,empleado.fmnombres as nombres,
		empleado.fmapellidopaterno as apellidopaterno,empleado.fmapellidomaterno as apellidomaterno,usuario.fmidusuario as idusuario,usuario.fmcorreo as correo,true as estadoflag,'credenciales correcta' as mensaje
		from fmempleado as empleado
		inner join fmempleado_usuario as empleadousuario on empleadousuario.fmidmempleado=empleado.fmidempleado
		inner join fmusuario as usuario on usuario.fmidusuario=empleadousuario.fmidusuario
		where usuario.fmcorreo=correo and  usuario.fmcontrasenia=md5(contrasenia);
    
    end if;	
    
	else
		select false as estadoflag,'Credenciales incorrecta.' as mensaje;
    end if;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `prInsertarCliente` (IN `nombres` VARCHAR(250), IN `correo` VARCHAR(250), IN `contrasenia` LONGTEXT, IN `apellidos` VARCHAR(250), IN `direccion` VARCHAR(250), IN `tipodocumento` VARCHAR(1), IN `tipocliente` VARCHAR(45), IN `distrito` VARCHAR(100), IN `numerocel` VARCHAR(25), IN `nombreempresa` VARCHAR(200), IN `idempleado` INT, IN `referencia` VARCHAR(500), IN `numerodocumento` INT(40))  BEGIN
	declare verusuario int ;
    declare idcliente int ;
    declare idusuario int;
    SET verusuario= 0;
    SET idcliente= 0;
    SELECT count(*) into verusuario  FROM fmusuario usuario where usuario.fmcorreo=correo;
	if verusuario>0 then
		select false as estado ,'El correo digitado ya están en uso.' as mensaje;
    else
		insert into fmcliente(fmnombrecompletos,
        fmapellidos,fmultimafechamodificacion
        ,fmdireccion,fmtipodocumento,fmtipocliente,fmnumerodocumento,
        fmdistrito,fmnumerocel,
        fmnombreempresa,fmidempleado,fmreferencia)
        values(nombres,apellidos,current_timestamp(),
        direccion,tipodocumento,tipocliente,
        numerodocumento,distrito,numerocel,
        nombreempresa,idempleado,referencia);
		SET idcliente=LAST_INSERT_ID();
		insert into fmusuario(fmcorreo,fmcontrasenia,fmestado,fmtipousuario,fmultimafechamodificacion)
        values(correo,MD5(contrasenia),true,'CLIENTE',current_timestamp());
        SET idusuario=LAST_INSERT_ID();
        insert into fmcliente_usuario(fmidcliente,fmidusuario)
        values(idcliente,idusuario);
		select true as estado ,'El usuario se ha creado con exito.' as mensaje;
    end if;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `prlistarcategorias` ()  BEGIN
   select categoria.fmidcategoria as id,categoria.fmnombre as nombre,categoria.fmdescripcion as descripcion,categoria.fmimg as img
   from fmcategoria as categoria where categoria.fmestado=true order by categoria.fmidcategoria asc;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `prmostrarclienteparaempleado` (IN `reidcliente` INT)  BEGIN
select usuario.fmidusuario as idusuario,usuario.fmcorreo as correo,usuario.fmestado as estado,usuario.fmtipousuario as tipousuario,
cliente.fmidcliente as idcliente,cliente.fmnombrecompletos as nombres,cliente.fmapellidos as apellidos,
cliente.fmfechacreacion as fechacreacion,cliente.fmultimafechamodificacion as fechaultimamodificacion,cliente.fmdireccion as direccion,
cliente.fmtipodocumento as tipodocumento,cliente.fmtipocliente as tipocliente,cliente.fmnumerodocumento as numerodocumento,cliente.fmdistrito as distrito,
cliente.fmnumerocel as numerocel,cliente.fmnombreempresa as nombreempresa,cliente.fmidempleado as idempleado,cliente.fmreferencia as referencia,
empleado.fmnombres as nombreempleadomultimamodificacion,empleado.fmapellidopaterno as apellidopaternoempleadomultimamodificacion,empleado.fmapellidomaterno as apellidomaternoempleadomultimamodificacion
 from fmusuario as usuario 
inner join fmcliente_usuario clienteusuario on clienteusuario.fmidusuario=usuario.fmidusuario
inner join fmcliente as cliente on cliente.fmidcliente=clienteusuario.fmidcliente
left join fmempleado as empleado on empleado.fmidempleado=cliente.fmidempleado
where cliente.fmidcliente=reidcliente;
   
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `prMostrarInformacionUsuario` (IN `estadopedido` VARCHAR(15), IN `idcliente` INT)  BEGIN
select cliente.fmnombrecompletos as nombrecompletos,cliente.fmapellidos as apellidos,pedido.fmidpedido as idpedido,
cliente.fmidcliente as idcliente, pedido.fmfecha as fecha,pedido.fmestado as estado ,
detalle.fmidpedido as idpedido,detalle.fmidproducto as idproducto,
producto.fmnombreproducto as producto,detalle.fmcantidad as cantidad,detalle.fmprecioventa as precioventa 
from fmpedidodetalle as detalle 
inner join fmproducto as producto on producto.fmidproducto=detalle.fmidproducto
inner join fmpedido as pedido on pedido.fmidpedido=detalle.fmidpedido
inner join fmcliente as cliente on cliente.fmidcliente=pedido.fmidcliente
where pedido.fmestado=coalesce(estadopedido,pedido.fmestado) and 
pedido.fmidcliente=coalesce(idcliente,pedido.fmidcliente);
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `prMostrarInformacionUsuarioPedidoDetalle` (IN `idcliente` INT)  BEGIN

select 
pedido.fmidempleadoUltimoModificacion as empleadoultimamodificacionidempleado,empleadoultimamodificacion.fmnombres as empleadoultimamodificacionnombres,empleadoultimamodificacion.fmapellidopaterno as empleadoultimamodificacionapellidopaterno,empleadoultimamodificacion.fmapellidomaterno as empleadoultimamodificacionapellidomaterno,
empleado.fmidempleado as empleadoregistroidempleado,empleado.fmnombres as  empleadoregistronombres,empleado.fmapellidopaterno as empleadoregistroapellidopaterno,empleado.fmapellidomaterno as empleadoregistromaterno,
cliente.fmnombrecompletos as nombrecompletos,cliente.fmapellidos as apellidos,pedido.fmidpedido as idpedido,
cliente.fmidcliente as idcliente,
pedido.fmidpedido as idpedido, pedido.fmfecha as fecha,pedido.fmestado as estado,
detalle.fmidpedido as idpedido,detalle.fmidproducto as idproducto,
producto.fmnombreproducto as producto,detalle.fmcantidad as cantidad,detalle.fmprecioventa as precioventa ,pedido.fmultimafechamodificacion,pedido.fmtokenpago as tokenpago
from fmpedidodetalle as detalle 
inner join fmproducto as producto on producto.fmidproducto=detalle.fmidproducto
inner join fmpedido as pedido on pedido.fmidpedido=detalle.fmidpedido
inner join fmcliente as cliente on cliente.fmidcliente=pedido.fmidcliente
left join fmempleado as empleado on empleado.fmidempleado=pedido.fmidempleado
left join fmempleado as empleadoultimamodificacion on empleadoultimamodificacion.fmidempleado=pedido.fmidempleadoUltimoModificacion
where pedido.fmidcliente=coalesce(idcliente,pedido.fmidcliente);
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `prregistrarproducto` (IN `nombreproducto` VARCHAR(150), IN `descripcion` VARCHAR(500), IN `idcategoria` INT, IN `novedad` INT, IN `precioventa` DECIMAL, `ruta` LONGTEXT, IN `detalles` LONGTEXT, IN `idEmpleado` INT)  BEGIN
	declare verproducto int ;
     SET verproducto = 0;

    SELECT count(*) into verproducto  FROM fmproducto as producto where producto.fmnombreproducto=nombreproducto;
	if verproducto>0 then
		select false as estado ,'El nombre producto ya existe.' as mensaje;
    else
		insert into fmproducto(fmnombreproducto,fmdescripcion,fmidcategoria,fmnovedad,fmprecioventa,fmdetalles,fmestado,
        fmimg,fmidempleado,fmultimafechamodificacion)
        values(nombreproducto,descripcion,idcategoria,novedad,precioventa,detalles,true,ruta,idEmpleado,current_timestamp);
		select true as estado ,'El producto se registro con exito.' as mensaje;
    end if;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `prVerificarCorreoUsuario` (IN `correo` VARCHAR(250))  BEGIN
	declare verusuario int ;
    SET verusuario= 0;
    SELECT count(*) into verusuario  FROM fmusuario usuario where usuario.fmcorreo=correo;
	if verusuario>0 then
		select true as estado,'El correo digitado ya esta en uso.' as mensaje;
    else
		select false as estado,'El Correo no se encuentra registrado.' as mensaje;
    end if;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `realizarpedido` (IN `idempleado` INT, IN `idcliente` INT, IN `estado` VARCHAR(15), IN `tipopago` VARCHAR(45), IN `comentarios` VARCHAR(200), IN `tokenpago` LONGTEXT)  BEGIN 
    declare idpedido int;
	set idpedido=0;
	insert into fmpedido(fmestado,fmidempleado,fmidcliente,fmtipopago,fmultimafechamodificacion,fmcomentarios,fmidempleadoUltimoModificacion,fmtokenpago) 
    values(estado,idempleado,idcliente,tipopago,current_timestamp(),comentarios,idempleado,tokenpago);
	set idpedido=LAST_INSERT_ID();
	select idpedido as idpedido;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `realizarpedidodetalle` (IN `idpedido` INT, IN `idproducto` INT, `cantidad` INT)  BEGIN 
    declare precioventa decimal;
    set precioventa=0;
    select producto.fmprecioventa into precioventa from fmproducto as producto where fmidproducto=idproducto;
    insert into fmpedidodetalle(fmidpedido,fmidproducto,fmcantidad,fmprecioventa) 
    values(idpedido,idproducto,cantidad,precioventa);
    select true as estado,'El detalle se inserto con exito.' as mensaje;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `verificarDocumentoIdentidad` (IN `documento` VARCHAR(45))  BEGIN
declare verificardocumento  int;
set verificardocumento=0;
	select count(*) into verificardocumento from fmempleado where fmdocumentoidentidad=documento;
    if verificardocumento>0 then
		select true as estado,'El documento digitado ya se encuentra registrado.' as mensaje;
    else
      select false as estado,'El documento digitado no se encuentra registrado.' as mensaje;
    end if;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `verificarDocumentoIdentidadCliente` (IN `documento` VARCHAR(45))  BEGIN
declare verificardocumento  int;
set verificardocumento=0;
	select count(*) into verificardocumento from fmcliente where fmnumerodocumento=documento;
    if verificardocumento>0 then
		select true as estado,'El documento digitado ya se encuentra registrado.' as mensaje;
    else
      select false as estado,'El documento digitado no se encuentra registrado.' as mensaje;
    end if;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `verificarnombreproducto` (IN `nombreproducto` VARCHAR(150))  BEGIN
	declare verproducto int ;
     SET verproducto = 0;
    SELECT count(*) into verproducto  FROM fmproducto as producto where producto.fmnombreproducto=nombreproducto;
	if verproducto>0 then
		select true  AS estado,'El nombre digitado producto ya existe.' as mensaje;
    else
		select false AS estado,'El nombre digitado producto no existe.' as mensaje;
    end if;
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fmcategoria`
--

CREATE TABLE `fmcategoria` (
  `fmidcategoria` int(11) NOT NULL,
  `fmnombre` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fmdescripcion` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fmestado` tinyint(4) DEFAULT NULL,
  `fmidempleado` int(11) NOT NULL,
  `fmfechacreacion` timestamp NULL DEFAULT current_timestamp(),
  `fmultimafechamodificacion` datetime DEFAULT NULL,
  `fmimg` longtext COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `fmcategoria`
--

INSERT INTO `fmcategoria` (`fmidcategoria`, `fmnombre`, `fmdescripcion`, `fmestado`, `fmidempleado`, `fmfechacreacion`, `fmultimafechamodificacion`, `fmimg`) VALUES
(1, 'Panadería y Dulces', 'Dulces maria', 1, 1, '2020-07-09 19:24:40', '2020-07-09 14:24:05', 'img/1594330673857.jpg'),
(2, 'Pastas & Pizas', 'Todo bien, comida, atención, atmósfera italiana (el pianista muy bueno) excepto que la mesa donde nos ubicaron era algo incómoda ya que la gente pasaba a cada rato para ir a servirse el buffet, a veces chocaban o nos teniamos que mover seguido. Siempre es bueno sacrificar un poco de espacio para que', 1, 1, '2020-07-14 21:50:21', '2020-07-14 16:48:59', 'img/1594330673857.jpg'),
(3, 'Postres & Helados', 'Un postre helado es el nombre genérico para los postres hechos de líquidos congelados, semi-sólidos, y a veces incluso sólidos. Pueden ser a base de agua con sabor (sorbete, granizado, etc.), en purés de frutas, en leche y crema (la mayoría de helados), en crema (crema congelada y algunos helados), en mousse (semifríos), etc. En Reino Unido y Irlanda, donde el término \"postre\" es poco frecuente, los postres congelados se refieren como \"hielos\".', 1, 1, '2020-07-14 22:01:20', '2020-07-14 17:00:23', 'img/1594333431370.jpg'),
(4, 'Carnes & Parrillas', 'El brasero que soluciona todos tus problemas de encendido de carbón.\r\nOlvdídate del mechero, de soplar, de hacer aire o de usar alcohol para prender el carbón. Este brasero o generador de brasa facilita todo el proceso. Debido al sistema de tiro de una chimenea, logra convertir el carbón en brasa fácilmente.', 1, 1, '2020-07-14 22:03:04', '2020-07-14 17:01:50', 'img/1594333497723.jpg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fmcliente`
--

CREATE TABLE `fmcliente` (
  `fmidcliente` int(11) NOT NULL,
  `fmnombrecompletos` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fmapellidos` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fmfechacreacion` timestamp NULL DEFAULT current_timestamp(),
  `fmultimafechamodificacion` datetime DEFAULT NULL,
  `fmdireccion` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fmtipodocumento` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fmtipocliente` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fmnumerodocumento` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fmdistrito` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fmnumerocel` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fmnombreempresa` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fmidempleado` int(11) DEFAULT NULL,
  `fmreferencia` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `fmcliente`
--

INSERT INTO `fmcliente` (`fmidcliente`, `fmnombrecompletos`, `fmapellidos`, `fmfechacreacion`, `fmultimafechamodificacion`, `fmdireccion`, `fmtipodocumento`, `fmtipocliente`, `fmnumerodocumento`, `fmdistrito`, `fmnumerocel`, `fmnombreempresa`, `fmidempleado`, `fmreferencia`) VALUES
(46, 'cliente', 'cliente', '2020-07-16 14:17:35', '2020-07-16 09:17:35', 'direccion', 'D', 'REGISTRADO', '71004732', '010101', '123456789', 'empresa', NULL, 'direccion'),
(47, 'DANIEL', 'CARRION', '2020-07-16 15:59:20', '2020-07-16 23:13:35', 'HUASCAR', 'D', 'REGISTRADO', '22222222', '140202', '222222222', 'SOL GAS', NULL, 'ESQUINA DEL PARQUE'),
(48, 'DANIEL', 'RIVERA', '2020-07-17 04:18:24', '2020-07-16 23:28:19', 'TALIO', 'D', 'REGISTRADO', '77889944', '010117', '987654321', NULL, NULL, 'PARQUE LOS ALAMOS'),
(49, 'GARCIA@GMAIL.COM', 'GARCIA@GMAIL.COM', '2020-07-17 09:05:52', '2020-07-17 04:05:52', '22222222222', NULL, 'REGISTRADO', '111', '010101', '122222222', NULL, NULL, '22222'),
(50, 'joseantonio', 'damian', '2020-07-17 09:17:20', '2020-07-17 04:17:20', 'DIRECCION', 'D', 'REGISTRADO', '15234567', '010101', '123456789', 'olmos', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fmcliente_usuario`
--

CREATE TABLE `fmcliente_usuario` (
  `fmidcliente` int(11) NOT NULL,
  `fmidusuario` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `fmcliente_usuario`
--

INSERT INTO `fmcliente_usuario` (`fmidcliente`, `fmidusuario`) VALUES
(46, 42),
(47, 43),
(48, 44),
(49, 45),
(50, 46);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fmempleado`
--

CREATE TABLE `fmempleado` (
  `fmidempleado` int(11) NOT NULL,
  `fmdocumentoidentidad` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `fmnombres` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `fmapellidopaterno` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `fmapellidomaterno` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `fmdistrito` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fmnumerocel` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fmtipodocumento` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fmfechacreacion` timestamp NULL DEFAULT current_timestamp(),
  `fmultimafechamodificacion` datetime DEFAULT NULL,
  `fmempleadocreador` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `fmempleado`
--

INSERT INTO `fmempleado` (`fmidempleado`, `fmdocumentoidentidad`, `fmnombres`, `fmapellidopaterno`, `fmapellidomaterno`, `fmdistrito`, `fmnumerocel`, `fmtipodocumento`, `fmfechacreacion`, `fmultimafechamodificacion`, `fmempleadocreador`) VALUES
(1, '71004732', 'JUAN', 'RAMIREZ', 'SANCHEZ', 'JOSE LEONARDO ORTIZ', '928206110', 'DNI', '2020-07-09 16:08:42', '2020-07-09 11:07:55', NULL),
(2, '123456789', 'EMPLEADO', 'EMPLEADO', 'EMPLEADO', 'EMPLEADO', '123456789', 'DNI', '2020-07-10 16:27:08', '2020-07-10 11:26:25', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fmempleado_usuario`
--

CREATE TABLE `fmempleado_usuario` (
  `fmidmempleado` int(11) NOT NULL,
  `fmidusuario` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `fmempleado_usuario`
--

INSERT INTO `fmempleado_usuario` (`fmidmempleado`, `fmidusuario`) VALUES
(1, 1),
(2, 18);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fmpedido`
--

CREATE TABLE `fmpedido` (
  `fmidpedido` int(11) NOT NULL,
  `fmfecha` timestamp NOT NULL DEFAULT current_timestamp(),
  `fmestado` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `fmidempleado` int(11) DEFAULT NULL,
  `fmidcliente` int(11) NOT NULL,
  `fmtipopago` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fmultimafechamodificacion` datetime DEFAULT NULL,
  `fmcomentarios` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fmidempleadoUltimoModificacion` int(11) DEFAULT NULL,
  `fmtokenpago` longtext COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `fmpedido`
--

INSERT INTO `fmpedido` (`fmidpedido`, `fmfecha`, `fmestado`, `fmidempleado`, `fmidcliente`, `fmtipopago`, `fmultimafechamodificacion`, `fmcomentarios`, `fmidempleadoUltimoModificacion`, `fmtokenpago`) VALUES
(28, '2020-07-16 14:17:58', '1', NULL, 46, 'visa', '2020-07-16 09:17:58', 'COMENTARIO', NULL, 'asas4a5s45a4s5a4sa5s4a5s4a54s4sa5s4'),
(29, '2020-07-16 15:16:48', '1', NULL, 46, 'visa', '2020-07-16 10:16:48', NULL, NULL, 'asas4a5s45a4s5a4sa5s4a5s4a54s4sa5s4'),
(30, '2020-07-16 16:00:37', '1', NULL, 47, 'visa', '2020-07-16 11:00:37', 'PRIMER ENVIO', NULL, 'asas4a5s45a4s5a4sa5s4a5s4a54s4sa5s4'),
(31, '2020-07-16 21:50:20', '1', NULL, 47, 'visa', '2020-07-16 16:50:20', 'JEJEJEJJEJEJEJ', NULL, 'asas4a5s45a4s5a4sa5s4a5s4a54s4sa5s4'),
(32, '2020-07-17 04:15:06', '1', NULL, 47, 'visa', '2020-07-16 23:15:06', 'REGISTRO DE VENTA GRACIAS', NULL, 'asas4a5s45a4s5a4sa5s4a5s4a54s4sa5s4'),
(33, '2020-07-17 07:59:05', '1', NULL, 48, 'visa', '2020-07-17 02:59:05', NULL, NULL, 'asas4a5s45a4s5a4sa5s4a5s4a54s4sa5s4'),
(34, '2020-07-17 09:03:21', '1', NULL, 48, 'visa', '2020-07-17 04:03:21', NULL, NULL, 'asas4a5s45a4s5a4sa5s4a5s4a54s4sa5s4'),
(35, '2020-07-17 09:25:48', '1', NULL, 50, 'visa', '2020-07-17 04:25:48', NULL, NULL, 'asas4a5s45a4s5a4sa5s4a5s4a54s4sa5s4');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fmpedidodetalle`
--

CREATE TABLE `fmpedidodetalle` (
  `fmidpedido` int(11) NOT NULL,
  `fmidproducto` int(11) NOT NULL,
  `fmcantidad` int(11) NOT NULL,
  `fmprecioventa` decimal(10,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `fmpedidodetalle`
--

INSERT INTO `fmpedidodetalle` (`fmidpedido`, `fmidproducto`, `fmcantidad`, `fmprecioventa`) VALUES
(28, 3, 5, '12.00'),
(28, 4, 1, '12.00'),
(28, 5, 1, '12.00'),
(28, 6, 1, '12.00'),
(29, 3, 3, '12.00'),
(30, 3, 1, '12.00'),
(30, 4, 1, '12.00'),
(31, 3, 4, '12.00'),
(32, 3, 2, '12.00'),
(33, 3, 5, '12.00'),
(33, 4, 1, '12.00'),
(33, 5, 1, '12.00'),
(33, 6, 3, '12.00'),
(34, 3, 1, '2.00'),
(34, 4, 3, '7.00'),
(34, 5, 2, '1.00'),
(34, 6, 3, '3.00'),
(35, 3, 1, '2.00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fmproducto`
--

CREATE TABLE `fmproducto` (
  `fmidproducto` int(11) NOT NULL,
  `fmnombreproducto` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `fmdescripcion` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fmidcategoria` int(11) NOT NULL,
  `fmnovedad` int(11) NOT NULL,
  `fmprecioventa` decimal(10,2) NOT NULL,
  `fmdetalles` longtext COLLATE utf8_unicode_ci NOT NULL,
  `fmimg` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `fmfechacreacion` timestamp NULL DEFAULT current_timestamp(),
  `fmultimafechamodificacion` datetime DEFAULT NULL,
  `fmidempleado` int(11) NOT NULL,
  `fmestado` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `fmproducto`
--

INSERT INTO `fmproducto` (`fmidproducto`, `fmnombreproducto`, `fmdescripcion`, `fmidcategoria`, `fmnovedad`, `fmprecioventa`, `fmdetalles`, `fmimg`, `fmfechacreacion`, `fmultimafechamodificacion`, `fmidempleado`, `fmestado`) VALUES
(3, 'HARINA', 'DESCRIPCION NUEVA PORQUE', 1, 0, '2.00', '[{\"titulo\":\"titt\",\"descripcion\":\"titt\"},{\"titulo\":\"titt\",\"descripcion\":\"titt\"}]', 'img/1594333837430.jpg', '2020-07-09 22:30:37', '2020-07-09 17:30:37', 1, 1),
(4, 'IMAGEN', 'DESCRIPCION NUEVA PORQUE', 4, 0, '7.00', '[{\"titulo\":\"titt\",\"descripcion\":\"titt\"},{\"titulo\":\"titt\",\"descripcion\":\"titt\"}]', 'img/1594398601482.jpg', '2020-07-10 16:30:01', '2020-07-10 11:30:01', 1, 1),
(5, 'IMAGEN1', 'DESCRIPCION NUEVA PORQUE', 3, 0, '1.00', '[{\"titulo\":\"titt\",\"descripcion\":\"titt\"},{\"titulo\":\"titt\",\"descripcion\":\"titt\"}]', 'img/1594442679715.jpg', '2020-07-11 04:44:39', '2020-07-10 23:44:39', 1, 1),
(6, 'IMAGEN2', 'DESCRIPCION NUEVA PORQUE', 2, 0, '3.00', '[{\"titulo\":\"titt\",\"descripcion\":\"titt\"},{\"titulo\":\"titt\",\"descripcion\":\"titt\"}]', 'img/1594442788607.jpg', '2020-07-11 04:46:28', '2020-07-10 23:46:28', 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fmusuario`
--

CREATE TABLE `fmusuario` (
  `fmidusuario` int(11) NOT NULL,
  `fmcorreo` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `fmcontrasenia` longtext COLLATE utf8_unicode_ci NOT NULL,
  `fmestado` tinyint(4) NOT NULL,
  `fmtipousuario` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `fmfechacreacion` timestamp NULL DEFAULT current_timestamp(),
  `fmultimafechamodificacion` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `fmusuario`
--

INSERT INTO `fmusuario` (`fmidusuario`, `fmcorreo`, `fmcontrasenia`, `fmestado`, `fmtipousuario`, `fmfechacreacion`, `fmultimafechamodificacion`) VALUES
(1, 'ARIESJUAN1996@GMAIL.COM', 'a94652aa97c7211ba8954dd15a3cf838', 1, 'EMPLEADO', '2020-07-09 16:11:40', '2020-07-09 11:09:59'),
(18, 'EMPLEADO@GMAIL.CON', 'd97224937f87e701b0e6527311843a93', 1, 'EMPLEADO', '2020-07-10 16:28:24', '2020-07-10 11:27:17'),
(42, 'CLIENTE@MAIL.COM', '5664fe282f20120db1eb7e7025dbafbf', 1, 'CLIENTE', '2020-07-16 14:17:35', '2020-07-16 09:17:35'),
(43, 'ATHUALPA@GMAIL.COM', '36be98ee2f67233db98eaacc099d4d36', 1, 'CLIENTE', '2020-07-16 15:59:20', '2020-07-16 23:13:35'),
(44, 'GARCIA@GMAIL.COM', '96e429b71f44e6f7ed08be00d1a3f627', 1, 'CLIENTE', '2020-07-17 04:18:24', '2020-07-16 23:28:19'),
(45, 'GARCIA12@GMAIL.COM', '96e429b71f44e6f7ed08be00d1a3f627', 1, 'CLIENTE', '2020-07-17 09:05:53', '2020-07-17 04:05:53'),
(46, 'DAMIAN@GMAIL.COM', '15626c5e0c749cb912f9d1ad48dba440', 1, 'CLIENTE', '2020-07-17 09:17:20', '2020-07-17 04:17:20');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `fmcategoria`
--
ALTER TABLE `fmcategoria`
  ADD PRIMARY KEY (`fmidcategoria`),
  ADD KEY `fk_fmcategoria_fmempleado1_idx` (`fmidempleado`);

--
-- Indices de la tabla `fmcliente`
--
ALTER TABLE `fmcliente`
  ADD PRIMARY KEY (`fmidcliente`),
  ADD KEY `fk_fmcliente_fmempleado1_idx` (`fmidempleado`);

--
-- Indices de la tabla `fmcliente_usuario`
--
ALTER TABLE `fmcliente_usuario`
  ADD PRIMARY KEY (`fmidcliente`,`fmidusuario`),
  ADD KEY `fk_fmcliente_has_fmusuario_fmusuario1_idx` (`fmidusuario`),
  ADD KEY `fk_fmcliente_has_fmusuario_fmcliente_idx` (`fmidcliente`);

--
-- Indices de la tabla `fmempleado`
--
ALTER TABLE `fmempleado`
  ADD PRIMARY KEY (`fmidempleado`),
  ADD KEY `fk_fmempleado_fmempleado1_idx` (`fmempleadocreador`);

--
-- Indices de la tabla `fmempleado_usuario`
--
ALTER TABLE `fmempleado_usuario`
  ADD PRIMARY KEY (`fmidmempleado`,`fmidusuario`),
  ADD KEY `fk_fmempleado_has_fmusuario_fmusuario1_idx` (`fmidusuario`),
  ADD KEY `fk_fmempleado_has_fmusuario_fmempleado1_idx` (`fmidmempleado`);

--
-- Indices de la tabla `fmpedido`
--
ALTER TABLE `fmpedido`
  ADD PRIMARY KEY (`fmidpedido`),
  ADD KEY `fk_fmpedido_fmempleado1_idx` (`fmidempleado`),
  ADD KEY `fk_fmpedido_fmcliente1_idx` (`fmidcliente`);

--
-- Indices de la tabla `fmpedidodetalle`
--
ALTER TABLE `fmpedidodetalle`
  ADD PRIMARY KEY (`fmidpedido`,`fmidproducto`),
  ADD KEY `fk_fmpedido_has_fmproducto_fmproducto1_idx` (`fmidproducto`),
  ADD KEY `fk_fmpedido_has_fmproducto_fmpedido1_idx` (`fmidpedido`);

--
-- Indices de la tabla `fmproducto`
--
ALTER TABLE `fmproducto`
  ADD PRIMARY KEY (`fmidproducto`),
  ADD KEY `fk_fmproducto_fmcategoria1_idx` (`fmidcategoria`),
  ADD KEY `fk_fmproducto_fmempleado1_idx` (`fmidempleado`);

--
-- Indices de la tabla `fmusuario`
--
ALTER TABLE `fmusuario`
  ADD PRIMARY KEY (`fmidusuario`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `fmcategoria`
--
ALTER TABLE `fmcategoria`
  MODIFY `fmidcategoria` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `fmcliente`
--
ALTER TABLE `fmcliente`
  MODIFY `fmidcliente` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT de la tabla `fmempleado`
--
ALTER TABLE `fmempleado`
  MODIFY `fmidempleado` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `fmpedido`
--
ALTER TABLE `fmpedido`
  MODIFY `fmidpedido` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT de la tabla `fmproducto`
--
ALTER TABLE `fmproducto`
  MODIFY `fmidproducto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `fmusuario`
--
ALTER TABLE `fmusuario`
  MODIFY `fmidusuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `fmcategoria`
--
ALTER TABLE `fmcategoria`
  ADD CONSTRAINT `fk_fmcategoria_fmempleado1` FOREIGN KEY (`fmidempleado`) REFERENCES `fmempleado` (`fmidempleado`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `fmcliente`
--
ALTER TABLE `fmcliente`
  ADD CONSTRAINT `fk_fmcliente_fmempleado1` FOREIGN KEY (`fmidempleado`) REFERENCES `fmempleado` (`fmidempleado`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `fmcliente_usuario`
--
ALTER TABLE `fmcliente_usuario`
  ADD CONSTRAINT `fk_fmcliente_has_fmusuario_fmcliente` FOREIGN KEY (`fmidcliente`) REFERENCES `fmcliente` (`fmidcliente`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_fmcliente_has_fmusuario_fmusuario1` FOREIGN KEY (`fmidusuario`) REFERENCES `fmusuario` (`fmidusuario`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `fmempleado`
--
ALTER TABLE `fmempleado`
  ADD CONSTRAINT `fk_fmempleado_fmempleado1` FOREIGN KEY (`fmempleadocreador`) REFERENCES `fmempleado` (`fmidempleado`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `fmempleado_usuario`
--
ALTER TABLE `fmempleado_usuario`
  ADD CONSTRAINT `fk_fmempleado_has_fmusuario_fmempleado1` FOREIGN KEY (`fmidmempleado`) REFERENCES `fmempleado` (`fmidempleado`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_fmempleado_has_fmusuario_fmusuario1` FOREIGN KEY (`fmidusuario`) REFERENCES `fmusuario` (`fmidusuario`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `fmpedido`
--
ALTER TABLE `fmpedido`
  ADD CONSTRAINT `fk_fmpedido_fmcliente1` FOREIGN KEY (`fmidcliente`) REFERENCES `fmcliente` (`fmidcliente`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_fmpedido_fmempleado1` FOREIGN KEY (`fmidempleado`) REFERENCES `fmempleado` (`fmidempleado`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `fmpedidodetalle`
--
ALTER TABLE `fmpedidodetalle`
  ADD CONSTRAINT `fk_fmpedido_has_fmproducto_fmpedido1` FOREIGN KEY (`fmidpedido`) REFERENCES `fmpedido` (`fmidpedido`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_fmpedido_has_fmproducto_fmproducto1` FOREIGN KEY (`fmidproducto`) REFERENCES `fmproducto` (`fmidproducto`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `fmproducto`
--
ALTER TABLE `fmproducto`
  ADD CONSTRAINT `fk_fmproducto_fmcategoria1` FOREIGN KEY (`fmidcategoria`) REFERENCES `fmcategoria` (`fmidcategoria`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_fmproducto_fmempleado1` FOREIGN KEY (`fmidempleado`) REFERENCES `fmempleado` (`fmidempleado`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
