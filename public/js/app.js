const productos = document.getElementById('panels');
const resumenFinalizar = document.getElementById('resumen-fin');
const body = document.getElementsByTagName("body")[0];
const html = document.getElementsByTagName("html")[0];



$(document).ready(function() {

    var scrollLink = $('.menu');
    var scrollLinkCom = $('.linkCom');
    // const owlnav = document.querySelector('#slider-options .owl-nav');

    // owlnav.classList.remove('disabled');
    // $('#slider-options .owl-nav button').click(function(e) {
    //     owlnav.classList.remove('disabled');

    // });

    // Smooth scrolling
    scrollLink.click(function(e) {
        e.preventDefault();
        // var headerHeight = $("div#slider-options").height();
        // console.log(headerHeight);
        $('body,html').animate({
            scrollTop: $(this.hash).offset().top - 120
        }, 1000);
    });
    // Smooth scrolling
    scrollLinkCom.click(function(e) {
        e.preventDefault();
        // $(this).parent().addClass('active-btn');
        // $(this).parent().siblings().removeClass('active-btn');
        $('body,html').animate({
            scrollTop: $(this.hash).offset().top - 65
        }, 1000);
    });
    scrollLink[0].classList.add('active-btn');

    // Active link switching
    $(window).scroll(function() {
        var scrollbarLocation = $(this).scrollTop();
        scrollLink[0].classList.remove('active-btn');
        scrollLink.each(function() {

            var sectionOffset = $(this.hash).offset().top - 120;

            if (sectionOffset <= scrollbarLocation) {
                $(this).parent().addClass('active-btn');

                // $(this).parent().children().addClass('white');

                $(this).parent().siblings().removeClass('active-btn');
                // $(this).parent().siblings().removeClass('transform');
            }
        })

    })


})

cargarEventListener();

function cargarEventListener() {

    mostrarForm_modal();
    mostrarCarritoHeader_modal();
    mostrarFormSugg();
    mostrarSearch();
    //resumenFinalizar.addEventListener('click', mostrarDetallesProd);
}

function mostrarForm_modal() {
    $('.fa-user-circle').click(function(e) {
        e.preventDefault();
        // console.log(':D');
        let token=localStorage.getItem("token");
        if(token!=null){
            alert("Ya tiene session");
            return false;     
        }
        const modalLogin = document.getElementById('modal-login');
        // const formSesion = document.getElementById('form-box');
        const btnLogin = document.getElementById('btn-login');
        const btnRegister = document.getElementById('btn-register');
        const formLogin = document.getElementById('login');
        const formRegister = document.getElementById('register');

        modalLogin.classList.add('carrito-mostrar');
        body.style.overflow = "hidden";

        $("#btn-register").click(function() {
            //Botones
            btnRegister.classList.add('activo-btn');
            btnLogin.classList.remove('activo-btn');

            //Formularios
            formLogin.classList.add('ocultar-form');
            formRegister.classList.add('mostrar-form');
        });
        $("#btn-login").click(function() {
            //Botones
            btnLogin.classList.add('activo-btn');
            btnRegister.classList.remove('activo-btn');

            //Formularios
            formRegister.classList.remove('mostrar-form');
            formLogin.classList.remove('ocultar-form');
        });
        $('.btn-close-carrito').click(function(e) {
            modalLogin.classList.remove('carrito-mostrar');
            body.style.overflow = "visible";
        });
    });
}

function mostrarCarritoHeader_modal() {
    $('.cart').click(function(e) {
        e.preventDefault();
        const modalCarrito = document.getElementById('modal-carrito-header');
        modalCarrito.classList.add('carrito-mostrar');
        body.style.overflow = "hidden";

        $('.btn-close-carrito').click(function(e) {
            modalCarrito.classList.remove('carrito-mostrar');
            body.style.overflow = "visible";
        });
    });

}

function mostrarFormSugg(){
    $('.sugg').click(function(e) {
        
        e.preventDefault();
//        console.log(':D');
        
        const modal = document.getElementById('modal-sugerencias');
        
        modal.classList.add('carrito-mostrar');
        body.style.overflow = "hidden";

        $('.btn-close').click(function(e) {
            modal.classList.remove('carrito-mostrar');
            body.style.overflow = "visible";
        });
    });
    
}
function mostrarSearch(){
    $('.toggle-btn-nav').click(function(e) {
        e.preventDefault();
    //    console.log(':D');
        
        const modal = document.getElementById('toggle-box-search');
        const input = document.querySelector('.search-txt');
        
        modal.classList.toggle('toogle-mostrar');
        input.style.display ="block";
        
        $('.search-txt').css({
            color: '#222'
        });
        $('.toggle-box-search .search-btn .fa').css({
            color: '#222'
        });
    });
    
}