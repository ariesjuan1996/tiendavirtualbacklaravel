function api(url,typePeticion,body,ContentType="application/json"){
    let token=localStorage.getItem("token");
    if(typePeticion=="GET"){
        let headerSend={
            'Content-Type': ContentType,
            'Authorization': 'Bearer '+token
        };
        if(token==null){
            headerSend={
                'Content-Type': ContentType
            };
        }
        return fetch(url, {
            method: "GET",
            headers: headerSend
        })
        .then(response => {
            return response.text();
        })
        .catch(error => {
            return error;
        });
    }else{
        let headerSend={
            'Content-Type': ContentType,
            'Authorization': 'Bearer '+token
        };
        if(token==null){
            headerSend={
                'Content-Type': ContentType
            };
        }
        return fetch(url, {
            method: "POST",
            body: JSON.stringify(body),
            headers: headerSend
        })
        .then(response => {
            return response.text();
        })
        .catch(error => {
            return error;
        });
    }

}