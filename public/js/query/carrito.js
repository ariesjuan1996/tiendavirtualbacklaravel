
const itemProducto= document.getElementsByClassName('itemproducto');

window.addEventListener("load", function(){
    anadirEventoClick();
    cargarDataCarrito();
    tabPanelProductos();
});
function anadirProductoBlur(e) {
    console.log("e: ",e);
    ejquery=$(e);
    if(ejquery.val()=="" || ejquery.val()<=0 ){
        ejquery.val(1);
        anadirProducto(1,ejquery.data().precio,ejquery.data().id,ejquery.data().nombre,ejquery.data().img);
    }else {
        anadirProducto(ejquery.val(),ejquery.data().precio,ejquery.data().id,ejquery.data().nombre,ejquery.data().img);
    }
}
function anadirEventoClick(){
  
    var elements = document.querySelectorAll('.itemproducto');
    for(var i=0;i<elements.length;i++){
      let precioVenta=elements[i].getElementsByClassName("valuecantidad")[0].dataset.precio;
      let idProducto=elements[i].getElementsByClassName("valuecantidad")[0].dataset.id;
      let nombre=elements[i].getElementsByClassName("valuecantidad")[0].dataset.nombre;
      let img=elements[i].getElementsByClassName("valuecantidad")[0].dataset.img;
      let input=document.getElementsByClassName('itemproducto')[i].getElementsByClassName("valuecantidad");
      idProducto=parseInt(idProducto);
      elements[i].addEventListener('click',(e)=> {
          
         anadirProducto(input[0].value,precioVenta,idProducto,nombre,img);
       });

    }
    var elementsClickDetalle= document.getElementsByClassName("anadirProducto");
  
    for(var i=0;i<elementsClickDetalle.length;i++){
        let precioVenta=elements[i].getElementsByClassName("valuecantidad")[0].dataset.precio;
        let idProducto=elements[i].getElementsByClassName("valuecantidad")[0].dataset.id;
        let nombre=elements[i].getElementsByClassName("valuecantidad")[0].dataset.nombre;
        let img=elements[i].getElementsByClassName("valuecantidad")[0].dataset.img;
        elementsClickDetalle[i].addEventListener('click',(e)=> {
            let indexProducto=parseInt(e.target.dataset.indexProducto);
            let indexCategoria=parseInt(e.target.dataset.indexCategoria);
            anadirProducto($($('.classinputdetalle')[indexProducto+indexCategoria]).val(),precioVenta,idProducto,nombre,img);
            $($($("#precioVenta"+idProducto).parent()).parent()).addClass('mostrar-input');
            $($($("#precioVenta"+idProducto).parent()).parent().parent().children()[0]).addClass('ocultar');
            $("#precioVenta"+idProducto).val($($('.classinputdetalle')[indexProducto+indexCategoria]).val());
            $('.mostrar-list').removeClass('mostrar-list');
            body.style.overflow = "visible";
            
            /*
            
            
            
          
            input.value=inputDetalle.value;
            const btn=document.getElementsByClassName("agregar")[indexCategoria+indexProducto];
            const btnParent = btn.parentElement;
            btnParent.lastElementChild.classList.add('mostrar-input');*/

            //location.reload();
          } );

    }
}
function  tabPanelProductos() {
    var listProductosLocal=localStorage.getItem("productoList");
    let classProducto=document.getElementsByClassName("agregar");
    if(listProductosLocal!=null){
        var productoParse=JSON.parse(listProductosLocal);
        console.log("productoParse",productoParse);
        for (let index = 0; index < productoParse.length; index++) {
            for (let indexClass = 0; indexClass < classProducto.length; indexClass++) {
               
                const element = classProducto[indexClass];
                let id=element.dataset.id;
                if(id==productoParse[index].id){
                    element.classList.add('ocultar');
                    const btnParent = element.parentElement
                    btnParent.lastElementChild.classList.add('mostrar-input');
      
                    console.log($("#precioVenta"+id));
                    $("#precioVenta"+id).val(productoParse[index].cantidad);
                }

            }  
        }
      
    }



    
 
}

function cargarDataCarrito(){
    let listProductos=localStorage.getItem("productoList");
    let domListProductoCarrito=document.getElementById("listProductoCarrito");
    let domCantidadProducto=document.getElementById("cantidadProducto");
    let domPrecioSubTotal=document.getElementById("precioSubTotal");
    let domPrecioTotal=document.getElementById("precioTotal");
    let htmlDom="";
    if(listProductos==null){
        document.getElementsByClassName("priceTotal")[0].innerHTML=0;
        document.getElementsByClassName("numTotal")[0].innerHTML=0;
        htmlDom="";
    }else{
       
        let sumTotal=0.0;
        let productoParse=JSON.parse(listProductos);
        let cantidaProductoTotal=0;
        productoParse.map((value)=>{
            console.log("value",value.img);
            console.log("listProductos",value);
            cantidaProductoTotal=cantidaProductoTotal+parseInt(value.cantidad);
            let img=value.img;
            let itemProductoPrecio=parseFloat(value.precioVenta);
            let cantidad=parseInt(value.cantidad);
            sumTotal=sumTotal+parseFloat(value.precioVenta)*parseInt(value.cantidad);
            htmlDom=htmlDom+`<li class="item-cart">
            <div class="item-product">
                <div class="img-item">
                    <img src="${img}" alt="" />
                </div>
                <div class="txt-item">
                    <div class="name">
                        <span class="cantidad">${cantidad}</span><span class="">X</span>
                        <a href="#">${value.nombre}</a>
                    </div>

                    <p class="price">${itemProductoPrecio}</p>
                </div>
                <div class="remove-prod">
                    <a class="">&times;</a>
                </div>
            </div>
        </li>`;
        });
        domListProductoCarrito.innerHTML=htmlDom;
        document.getElementsByClassName("priceTotal")[0].innerHTML=sumTotal;
        document.getElementsByClassName("numTotal")[0].innerHTML=productoParse.length;
        domCantidadProducto.innerHTML=cantidaProductoTotal;
        domPrecioSubTotal.innerHTML=sumTotal;
        domPrecioTotal.innerHTML=sumTotal;
        //htmlDom="";
    }
}
function anadirProducto(cantidad,precioVenta,id,nombre,img){

    var listProductos=localStorage.getItem("productoList");
    if(listProductos==null){
        listProductos=[];
        listProductos.push({
            img:img,
            nombre:nombre,
            precioVenta:precioVenta,
            cantidad:cantidad,
            id:id,
        });
        localStorage.setItem("productoList",JSON.stringify(listProductos));
    }else{
 
        var productoParse=JSON.parse(listProductos);
        var bandIndex=null;
        for (let index = 0; index < productoParse.length; index++) {
            if(productoParse[index].id==id){
                bandIndex=index;
            }
            
        }
      
        if(bandIndex==null){
            productoParse.push({
                img:img,
                nombre:nombre,
                precioVenta:precioVenta,
                cantidad:cantidad,
                id:id
            });
        }else{
            productoParse[bandIndex].cantidad=cantidad;
            productoParse[bandIndex].precioVenta=precioVenta;
            productoParse[bandIndex].id=id;
        }
        localStorage.setItem("productoList",JSON.stringify(productoParse));
    }
    cargarDataCarrito();
    return true;
}