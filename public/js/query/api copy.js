function api(url,typePeticion,body,ContentType="application/json"){
    let token=localStorage.getItem("token");
    let headerSend= {
        method: typePeticion,
        headers: {
            'Content-Type':ContentType
        }
    };
    if(token!=null){
        headerSend= {
            method: typePeticion,
            headers: {
                'Authorization': 'Bearer '+token,
                'Content-Type':ContentType
            }
        };
    }
    if(typePeticion=="GET"){

        return fetch(url, {
            method: "GET",
            headers: headerSend
        })
        .then(response => {
            return response.text();
        })
        .catch(error => {
            return error;
        });
    }else{
        console.log("headerSend",headerSend);
        console.log("body",body);
        return fetch(url, {
            method: "POST",
            body: JSON.stringify(body),
            headers: headerSend
        })
        .then(response => {
            return response.text();
        })
        .catch(error => {
            return error;
        });
    }

}