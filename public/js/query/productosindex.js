window.addEventListener("load", function(){
    cargarProductos();
});
function cargarProductos(){
    api("api/categorias","GET",{}).then((data)=>{
        let dataProducto=JSON.parse(data);
        let estado=dataProducto.estado;
        
        if(estado){
          let categorias=dataProducto.data;
          console.log(categorias);
          let htmlData="";
          let htmlDataConImg="";
 
          categorias.map((value)=>{
            htmlData=htmlData+`<li class="item-nav center">
                <a class="" href="#">${value.nombre}</a>
            </li>`;
            htmlDataConImg=htmlDataConImg+`<a class="item-banner col" href="productos">
            <img src="${value.img}" alt="">
            <div class="pie-banner center">
                <p>${value.nombre}</p>
            </div>
            </a>`;
            });
         
          document.getElementById("itemscategorias").innerHTML=htmlData;
          document.getElementById("categoriasPorImagen").innerHTML=htmlDataConImg;
        }else{
            document.getElementById("itemscategorias").innerHTML=``;
            document.getElementById("categoriasPorImagen").innerHTML=``;
        }
        
    }).catch((error)=>{
      
        console.log(error);
         document.getElementById("itemscategorias").innerHTML=``;
    });
}