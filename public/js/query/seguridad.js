const modalLogin = document.getElementById('modal-login');
const form = document.getElementById('login');

//iniciarLogin
const formRegister = document.getElementById('register');
const inputCorreo = document.getElementById('emailRegistro');
const domUsuarioLogueado= document.getElementById('domUsuarioLogueado');
form.addEventListener('submit', iniciarSession);

formRegister.addEventListener('submit', registrarUsuario);
inputCorreo.addEventListener('blur', checkEmail);
window.addEventListener("load", function(){
    cargarDataUsuario();
});
function cargarDataUsuario(){
   let token=localStorage.getItem("token");
   let dataUsuario=localStorage.getItem("dataUsuario");

   if(token!=null){ 
    api("api/verificarSession","GET",{})
    .then((data)=>{
        let dataApi=JSON.parse(data);
        let estado=dataApi.estado;
        
        if(estado){
          let dataParse=JSON.parse(dataUsuario);
          if(dataParse.tipoUsuario=="EMPLEADO"){
            let nombreCompletos=dataParse.nombres;
            let apellidoMaterno=dataParse.apellidoMaterno;
            let apellidoPaterno=dataParse.apellidoPaterno;
            let documentoIdentidad=dataParse.documentoIdentidad;
            let idEmpleado=dataParse.idEmpleado;
            let idUsuario=dataParse.idUsuario;
            let tipoUsuario=dataParse.tipoUsuario;
            let correo=dataParse.correo;
            let correoAux=dataParse.correo.substr(0,10);
            domUsuarioLogueado.innerHTML=correoAux;
          }else{
            let nombreCompletos=dataParse.nombreCompletos;
            let apellidos=dataParse.apellidos;
            let correo=dataParse.correo;
            let idCliente=dataParse.idCliente;
            let idUsuario=dataParse.idUsuario;
            let tipoUsuario=dataParse.tipoUsuario;
            correoAux=dataParse.correo.substr(0,10);
            domUsuarioLogueado.innerHTML=correoAux;
          }

        }else{
            
            localStorage.removeItem("token");
            localStorage.removeItem("dataUsuario");
        }
    }).catch((error)=>{
      //  document.getElementById("itemscategorias").innerHTML=``;
    });  
   }
  
}
function registrarUsuario(e){
    e.preventDefault();
    let nombresRegistro=document.getElementById("nombresRegistro").value;
    let apellidosRegistro=document.getElementById("apellidosRegistro").value;
    let emailRegistro=document.getElementById("emailRegistro").value;
    let contraseniaRegistro=document.getElementById("contraseniaRegistro").value;
    let requestData={
        "nombre":nombresRegistro,
        "email":emailRegistro,
        "apellidos":apellidosRegistro,
        "constrasenia":contraseniaRegistro
    };
    api("api/registraCliente","POST",requestData).then((data)=>{
        let dataApi=JSON.parse(data);
        console.log("dataApi",dataApi);
        let estado=dataApi.estado;
      
        if(estado){
            let token=dataApi.token;
            let dataUsuario=dataApi.data;
            let dataUsuarioString=JSON.stringify(dataUsuario);
            localStorage.setItem("token",token);
            localStorage.setItem("dataUsuario",dataUsuarioString);
            modalLogin.classList.remove('carrito-mostrar');
            body.style.overflow = "visible";
            cargarDataUsuario();
        }else{
            let mensaje=dataApi.mensaje;
            alert(mensaje);
        }
    }).catch((error)=>{
        console.log(error);
      
    });
}
function esIniciarSession(){
    let listClass=document.getElementById("btn-login").classList;
    for (let index = 0; index < listClass.length; index++) {
        if(listClass[index]=="activo-btn"){
            return true;
        }
        
    }
    return false;
}
function iniciarSession(e){
    e.preventDefault();
    let esSession=esIniciarSession();
    console.log("esSession",(esSession ? 'sii' : 'noo'));
    if(esSession){
        var email = document.getElementById("email").value;
        var contrasenia = document.getElementById("contrasenia").value;
        console.log(email,contrasenia);
        api("api/iniciarSession","POST",    {
            "email":email,
            "contrasenia":contrasenia
        }).then((data)=>{
            let dataApi=JSON.parse(data);
            let estado=dataApi.estado;
            if(estado){
                let token=dataApi.token;
                let dataUsuario=dataApi.data;
                let dataUsuarioString=JSON.stringify(dataUsuario);
                localStorage.setItem("token",token);
                localStorage.setItem("dataUsuario",dataUsuarioString);
                modalLogin.classList.remove('carrito-mostrar');
                body.style.overflow = "visible";
            }else{
                let mensaje=dataApi.mensaje;
                alert(mensaje);
            }
            location.reload();
        }).catch((error)=>{
            console.log(error);
          
        });
    }else{

    }


}
function checkEmail(str)
{
    let correo=inputCorreo.value;
    if(!correo){
        return false;
    }
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if(re.test(correo)){
        api("api/verificarCorreoUsuario?email="+correo,"GET",{}).then((data)=>{
            let dataApi=JSON.parse(data);
            let estado=dataApi.estado;
            if(estado){
                inputCorreo.value="";
                alert(dataApi.mensaje);
            }
            console.log(dataApi);
            
        }).catch((error)=>{
            console.log(error);
        });
    }else{
        alert("Format correo invalido");

    }
}
function verificarCorreo(e){
    e.preventDefault();

    var email = document.getElementById("email").value;
    var contrasenia = document.getElementById("contrasenia").value;
    console.log(email,contrasenia);
    api("api/verificarCorreoUsuario?email="+email,"GET",{}).then((data)=>{
        let dataApi=JSON.parse(data);
        let estado=dataApi.estado;
        console.log(dataApi);
        /*
        if(estado){
            let token=dataApi.token;
            let dataUsuario=dataApi.data;
            let dataUsuarioString=JSON.stringify(dataUsuario);
            localStorage.setItem("token",token);
            localStorage.setItem("dataUsuario",dataUsuarioString);
            modalLogin.classList.remove('carrito-mostrar');
            body.style.overflow = "visible";
        }else{
            let mensaje=dataApi.mensaje;
            alert(mensaje);
        }*/
        
    }).catch((error)=>{
        console.log(error);
    });



}