<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|listarDetalleProducto 
*/

Route::post('iniciarSession', 'SeguridadController@iniciarSession');
Route::get('filtrarProductos', 'ProductosController@filtrarProductos');
Route::get('categorias', 'ProductosController@categorias');
Route::get('listarDetalleProducto', 'ProductosController@listarDetalleProducto');
Route::post('registraCliente', 'ClienteController@registraCliente');
Route::get('verificarCorreoUsuario', 'UsuarioController@verificarCorreoUsuario');
//dataUbigeo
Route::get('dataUbigeo', 'ClienteController@dataUbigeo');
Route::get('cargarProvincia', 'ClienteController@cargarProvincias');
Route::get('cargarDepartamento', 'ClienteController@cargarDepartamento');
Route::get('cargarDistritos', 'ClienteController@cargarDistritos');
//sendEmail
Route::get('sendEmail', 'PedidoController@sendEmail');
Route::get('filtraCategoriasPorProducto', 'ProductosController@filtraCategoriasPorProducto');

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::group(['middleware' => ['rol']], function () {
    Route::get('verificarDocumentoIdentidad', 'EmpleadoController@verificarDocumentoIdentidad');
    Route::get('verificarNombreProducto', 'ProductosController@verificarNombreProducto');
    Route::post('registrarProducto', 'ProductosController@registraProducto');  
    Route::post('actualizarPedido', 'PedidoController@actualizarPedidos');
});

Route::group(['middleware' => ['autenticado']], function () {
    Route::get('cerrarSession', 'SeguridadController@cerrarSession');
    Route::get('mostrarCliente', 'ClienteController@mostrarCliente');
    Route::get('mostrarPedido', 'PedidoController@mostrarPedido');
    Route::post('realizarPedido', 'PedidoController@realizarPedido');
    Route::post('actualizarCliente', 'ClienteController@actualizarCliente');
    Route::get('verificarSession', 'SeguridadController@verificarSession');
});