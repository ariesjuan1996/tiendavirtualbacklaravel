<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('vista', 'NuevoController@vista');
//carrito
Route::get('/', 'ProductosController@indexPrincipal');
Route::get('productos', 'ProductosController@productos');
Route::get('carrito', 'ProductosController@indexCarrito');
Route::get('finalizar', 'ProductosController@finalizar');