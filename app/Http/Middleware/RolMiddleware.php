<?php

namespace App\Http\Middleware;

use Closure;
use JWTAuth;
class RolMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $datos=$request->bearerToken() ? $request->bearerToken() : null;

        if(is_null($datos)){
            return response()->json([
                'estado'=>false,
                'error'=>"se requiere de token",
                "mensaje" => 'Acceso no autorizado.'
            ]);
        }else{
           
           try {
           
            $res=JWTAuth::setToken($request->bearerToken())->getPayload();
           
            if($res['tipoUsuario']=="EMPLEADO"){
                return $next($request);    
            }else{
                return response()->json([
                    'estado'=>false,
                    "mensaje" => 'Usuario no tiene permiso para esta operacion.'
                ]);
            }
             } catch (\Throwable $e) {
          
               if (is_a($e, "Tymon\JWTAuth\Exceptions\TokenExpiredException") == 1) {
                return response()->json(array("estado"=>false,"mensaje"=>'token_expired',"error" => 'token_expired'), $e->getStatusCode());
                }else
                if (is_a($e, "Tymon\JWTAuth\Exceptions\TokenBlacklistedException") == 1) {
                return response()->json(array("estado"=>false,"mensaje"=>'token_blacklisted',"error" => 'token_blacklisted'), $e->getStatusCode());
                } else
                if (is_a($e, "Tymon\JWTAuth\Exceptions\TokenInvalidException") == 1) {
                return response()->json(array("estado"=>false,"mensaje"=>'token_invalid',"error" => 'token_invalid'), $e->getStatusCode());
                } else {
                return response()->json(array("estado"=>false,"mensaje"=>'Token is required',"error" => 'Token is required'));
                }
            }
            
        }

    }
}
