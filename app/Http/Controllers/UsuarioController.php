<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use JWTAuth;
use App\User;
class UsuarioController extends Controller
{
    public  function verificarCorreoUsuario(Request $request){
        try {
            $email=is_null($request->input("email")) || $request->input("email")==""  ? null :mb_strtoupper($request->input("email"));          
            if(!preg_match('/^([a-z0-9]+([_\.\-]{1}[a-z0-9]+)*){1}([@]){1}([a-z0-9]+([_\-]{1}[a-z0-9]+)*)+(([\.]{1}[a-z]{2,6}){0,3}){1}$/i', $email)){
                return response()->json([
                    "estado"=>false,
                    "mensaje"=>'Formato de correo invalido.',
                ]);
            } 
            $data = DB::select("CALL `prVerificarCorreoUsuario`(?);", array($email));
            if(empty($data)){
                return response()->json([
                    "estado"=>false,
                    "mensaje"=>"No hubo repuesta del servidor.",
                ]);
            }
            return response()->json([
                "estado"=>$data[0]->estado ? true : false,
                "mensaje"=>$data[0]->mensaje,
            ]);

    
        } catch (\Throwable $th) {
            //throw $th;
            return response()->json([
                'estado'=>false,
                'mensaje'=>$th->getMessage() ]);
        }
       
    }
}
