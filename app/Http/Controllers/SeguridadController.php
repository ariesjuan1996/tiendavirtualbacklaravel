<?php

namespace App\Http\Controllers;
use App\User;
use Illuminate\Http\Request;
use DB;
use JWTAuth;
class SeguridadController extends Controller
{
    public function iniciarSession(Request $request){
        
        try {
            $email=is_null($request->input("email")) || $request->input("email")==""  ? null :mb_strtoupper($request->input("email"));
            $contrasenia=is_null($request->input("contrasenia")) || $request->input("contrasenia")==""  ? null :$request->input("contrasenia");
            
            if(is_null($email) ){
                return response()->json([
                    "estado"=>false,
                    "mensaje"=>"Se atributo usuario requerido."
                ]);
            }
            if(mb_strlen($email)>250){
                return response()->json([
                    "estado"=>false,
                    "mensaje"=>"El atributo email sobrepasa la longitud definida."
                ]);
            }
            if(is_null($contrasenia) ){
                return response()->json([
                    "estado"=>false,
                    "mensaje"=>"Se requiere de contrasenia."
                ]);
            }
        $data = DB::select("call `prIniciarSession`(?,?);",array($email,$contrasenia));   
        if(empty($data)){
            return response()->json([
                "estado"=>false,
                "mensaje"=>"No se encontraron datos."
            ]);
        }
        $dataUsuario=$data[0];
        $dataResponse=null;
        if($dataUsuario->estadoflag){
            if($dataUsuario->tipousuario=="EMPLEADO"){
                $dataResponse=[
                    'tipoUsuario'=>$dataUsuario->tipousuario,
                    'idEmpleado'=>$dataUsuario->idempleado,
                    'documentoIdentidad'=> $dataUsuario->documentoidentidad,
                    'nombres'=>$dataUsuario->nombres,
                    'apellidoPaterno'=>$dataUsuario->apellidopaterno,
                    'apellidoMaterno'=>$dataUsuario->apellidomaterno,
                    'idUsuario'=>$dataUsuario->idusuario,
                    'correo'=>$dataUsuario->correo
                ]; 
            }else{
                $dataResponse=[
                    'tipoUsuario'=>$dataUsuario->tipousuario,
                    'idCliente'=>$dataUsuario->idcliente,
                    'nombreCompletos'=>$dataUsuario->nombrecompletos,
                    'apellidos'=>$dataUsuario->apellidos,
                    'idUsuario'=>$dataUsuario->idusuario,
                    'correo'=>$dataUsuario->correo
                ]; 
            }
    
            $user=new User();
            $tokenData = JWTAuth::fromUser($user, $dataResponse);
            $res=JWTAuth::setToken($tokenData)->getPayload();
           
            return response()->json([
                "estado"=>true,
                "data"=>$dataResponse,
                "token"=> $tokenData 
            ]);
        }else{
            return response()->json([
                "estado"=>false,
                "mensaje"=>$dataUsuario->mensaje,
            ]);           
        }

        } catch (\Throwable $th) {
            return response()->json([
                'estado'=>false,
                'mensaje'=>$th->getMessage()
            ]);
        }
 

    }
    public static function cerrarSession(Request $request){
        try {
            JWTAuth::invalidate($request->bearerToken());
            return response()->json([
                "estado"=>true,
                "mensaje"=>"Cerro la sessiòn con exìto."
            ]);            
        } catch (\Throwable $th) {
            return response()->json([
                "estado"=>false,
                "mensaje"=>"Hubo problemas al cerrar sessiòn."
            ]);
        }
        
    }
    public function verificarSession(){
        return response()->json([
            "estado"=>true,
            "mensaje"=>"ok"
        ]);
    }
}
