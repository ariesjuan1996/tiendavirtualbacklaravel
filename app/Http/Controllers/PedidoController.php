<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use JWTAuth;
use Mail;
use App\Mail\SendMail;

class PedidoController extends Controller
{
    public function sendEmail(Request $request){
        try {
            $data = array(
                'name'      =>  "HOLLA",
                'message'   =>   "heeee"
            );
            \Mail::send ( 'plantilla', $data, function ($sendemail) {

                $sendemail->from ( 'jramirez.leoncito@gmail.com', 'Me Team' );
    
                $sendemail->to ( 'jramirez.leoncito@gmail.com', '' )->subject ( 'Activate your account' );
    
            } );

            //return back()->with('success', 'Thanks for contacting us!');
        } catch (\Throwable $th) {
            dd($th);
        }
        dd("GHOLÑAAA");
        
    }
    public  function mostrarPedido(Request $request){
        try {
          $reponseJson=null;
           $decoded=JWTAuth::setToken($request->bearerToken())->getPayload();
         //dd($decoded);
          $idCliente=is_null($request->input("idCliente")) || $request->input("idCliente")==""  ? null :$request->input("idCliente");
         
          if ($decoded["tipoUsuario"]=="EMPLEADO"){
          
            if(!is_null($idCliente)){
         
            
                if(!preg_match("/^[0-9]+$/", $idCliente)){
                    return response()->json([
                         "estado"=>false,
                         "mensaje"=>"El idCliente  debe ser numerico."
                     ]);
                 }
            }
               
          }else{
            $idCliente=$decoded["idCliente"];
          }
          $data = DB::select("CALL `prMostrarInformacionUsuarioPedidoDetalle`(?);", array($idCliente));
          
          if(empty($data)){
              return response()->json([
                  'estado'=>false,
                  'mensaje'=>"NO se encontrados datos."
              ]);
          }
          $cabecera=[];
          $auxIdPedido=null;
          foreach ($data as  $detalle) {
             
              if($auxIdPedido!=$detalle->idpedido){
                array_push($cabecera,[
                    'ultimaModificacionIdempleado'=>$detalle->empleadoultimamodificacionidempleado,
                    'empleadoUltimaModificacionNombres'=>$detalle->empleadoultimamodificacionnombres,
                    'empleadoUltimaModificacionApellidoPaterno'=>$detalle->empleadoultimamodificacionapellidopaterno,
                    'empleadoUltimaModificacionApellidoMaterno'=>$detalle->empleadoultimamodificacionapellidomaterno,
                    'empleadoRegistroIdempleado'=>$detalle->empleadoregistroidempleado,
                    'empleadoRegistroNombres'=>$detalle->empleadoregistronombres,
                    'empleadoRegistroApellidoPaterno'=>$detalle->empleadoregistroapellidopaterno,
                    'empleadoRegistroMaterno'=>$detalle->empleadoregistromaterno,
                    'nombreCompletos'=>$detalle->nombrecompletos,
                    'clienteApellidos'=>$detalle->apellidos,
                    'idPedido'=>$detalle->idpedido,
                    'idCliente'=>$detalle->idcliente,
                    'fecha'=>$detalle->fecha,
                    'estado'=>$detalle->estado,
                    'ultimaFechaModificacion'=>$detalle->fmultimafechamodificacion,
                    'tokenPago'=>$detalle->tokenpago
                ]);    
                $auxIdPedido=$detalle->idpedido;
              }
            
        }

        
        $dataConDetalles=[];

        if ($decoded["tipoUsuario"]=="EMPLEADO"){
            foreach ($cabecera as $key => $value) {
                $detallesItem=[];
              
                foreach ($data as  $detalle) {
                    if($value["idPedido"]==$detalle->idpedido){
                      
                        array_push($detallesItem,[
                          
                            'idproducto'=>$detalle->idproducto,
                            'producto'=>$detalle->producto,
                            'cantidad'=>$detalle->cantidad,
                            'precioVenta'=>$detalle->precioventa,
                        ]);   
                    }
                   
                    array_push($dataConDetalles,[
                        'tokenPago'=>$detalle->tokenpago,
                        'ultimaModificacionIdempleado'=>$value['ultimaModificacionIdempleado'],
                        'empleadoUltimaModificacionNombres'=>$value['empleadoUltimaModificacionNombres'],
                        'empleadoUltimaModificacionApellidoPaterno'=>$value['empleadoUltimaModificacionApellidoPaterno'],
                        'empleadoUltimaModificacionApellidoMaterno'=>$value['empleadoUltimaModificacionApellidoMaterno'],
                        'empleadoRegistroIdempleado'=>$value['empleadoRegistroIdempleado'],
                        'empleadoRegistroNombres'=>$value['empleadoRegistroNombres'],
                        'empleadoRegistroApellidoPaterno'=>$value['empleadoRegistroApellidoPaterno'],
                        'empleadoRegistroMaterno'=>$value['empleadoRegistroMaterno'],
                        'nombrecompletos'=>$value['nombreCompletos'],
                        'clienteApellidos'=>$value['clienteApellidos'],
                        'idPedido'=>$value['idPedido'],
                        'idCliente'=>$value['idCliente'],
                        'fecha'=>$value['fecha'],
                        'estado'=>$value['estado'],
                        'ultimaFechaModificacion'=>$value['ultimaFechaModificacion'] ,
                         'detalles'=>$detallesItem                   
                        ]);
    
                }
            }
        }else{
            foreach ($cabecera as $key => $value) {
                $detallesItem=[];
              
                foreach ($data as  $detalle) {
                 
                    if($value["idPedido"]==$detalle->idpedido){
                      
                        array_push($detallesItem,[
                            
                            'idproducto'=>$detalle->idproducto,
                            'producto'=>$detalle->producto,
                            'cantidad'=>$detalle->cantidad,
                            'precioVenta'=>$detalle->precioventa,
                        ]);   
                    }
                   
                    array_push($dataConDetalles,[
                        'ultimaModificacionIdempleado'=>$value['ultimaModificacionIdempleado'],
                        'empleadoUltimaModificacionNombres'=>$value['empleadoUltimaModificacionNombres'],
                        'empleadoUltimaModificacionApellidoPaterno'=>$value['empleadoUltimaModificacionApellidoPaterno'],
                        'empleadoUltimaModificacionApellidoMaterno'=>$value['empleadoUltimaModificacionApellidoMaterno'],
                        'empleadoRegistroIdempleado'=>$value['empleadoRegistroIdempleado'],
                        'empleadoRegistroNombres'=>$value['empleadoRegistroNombres'],
                        'empleadoRegistroApellidoPaterno'=>$value['empleadoRegistroApellidoPaterno'],
                        'empleadoRegistroMaterno'=>$value['empleadoRegistroMaterno'],
                        'nombrecompletos'=>$value['nombreCompletos'],
                        'clienteApellidos'=>$value['clienteApellidos'],
                        'idPedido'=>$value['idPedido'],
                        'idCliente'=>$value['idCliente'],
                        'fecha'=>$value['fecha'],
                        'estado'=>$value['estado'],
                        'ultimaFechaModificacion'=>$value['ultimaFechaModificacion'] ,
                         'detalles'=>$detallesItem                   
                        ]);
    
                }
            }
        }
          return response()->json([
                'estado'=>true,
                'data'=>$dataConDetalles
            ]);
        } catch (\Throwable $e) {
         return response()->json([
              'estado'=>false,
              'mensaje'=>$e->getMessage()
          ]);
      }
      
          echo $reponseJson;
    }
    public  function mostrarPedidos($request){
      try {
          
         $idCliente=is_null($request->input("idCliente")) || $request->input("idCliente")==""  ? null :$request->input("idCliente");
         $estado=is_null($request->input("estado")) || $request->input("estado")==""  ? null :$request->input("estado");
      
          if(!is_null($idCliente)){
            if(!preg_match("/^[0-9]+$/", $idCliente)){
               return response()->json([
                    "estado"=>false,
                    "mensaje"=>"El id del producto debe  ser numerico."
                ]);
                }
          }else{
            $idCliente ="null";
          }
          if(!is_null($estado)){
            if(strlen($estado)>15) {
               return response()->json([
                    "estado"=>false,
                    "mensaje"=>"El estado no debe sobrepasar los 15 caracteres."
                ]);
            }
            $estado="'$estado'";
          }else{
            $estado="null";
          }
         $decoded=JWTAuth::setToken($request->bearerToken())->getPayload();

          $tipoUsuario=$decoded["tipoUsuario"];
          if ($tipoUsuario=="EMPLEADO"){

            $data = DB::select("CALL `prMostrarInformacionUsuario`(?,?);", array($estado,$idCliente));

            if(empty($data)){
                return response()->json([
                    'estado'=>false,
                    'mensaje'=>"NO da respuesta."
                ]);
            }

            $dataResponse=[];
           foreach ($data as  $detalle) {
                array_push($dataResponse,[
                    'nombreCompletos'=>$detalle->nombrecompletos,
                    'apellidos'=>$detalle->apellidos,
                    'idCliente'=>$detalle->idcliente,
                    'fecha'=>$detalle->fecha,
                    'estado'=>$detalle->estado,
                    'idPedido'=>$detalle->idpedido,
                    'idProducto'=>$detalle->idproducto,
                    'producto'=>$detalle->producto,
                    'cantidad'=>$detalle->cantidad,
                    'precioVenta'=>$detalle->precioventa

                ]);
            }
            return response()->json([
                'estado'=>true,
                'data'=>$dataResponse
            ]);
           
          }else{
           return response()->json([
                'estado'=>false,
                'mensaje'=>"Usuario no tiene permisos.",
            ]);   
          } 

        } catch (\Throwable $e) {
         return response()->json([
              'estado'=>false,
              'mensaje'=>$e->getMessage()
          ]);
      }
      
   
    }
    public function actualizarPedidos(Request $request){
        try {
         $idPedido=is_null($request->input("idPedido")) || $request->input("idPedido")==""  ? null :$request->input("idPedido");
         $estado=is_null($request->input("estado")) || $request->input("estado")==""  ? null :$request->input("estado");
      
        if(!preg_match("/^[0-9]+$/", $idPedido)){
            return response()->json([
                "estado"=>false,
                "mensaje"=>"El id del pedido debe  ser numerico."
            ]);
        }
        
          ///ESTADOS DE LOS PEDIDOS :NUEVOPEDIDO,ENTREGADO,CANCELADO
          if(!($estado=="PEDIDO" || $estado=="PAGADO" || $estado=="ENTREGADO" || $estado=="FINALIZADO")){
            return response()->json([
                "estado"=>false,
                "mensaje"=>"El valor de estado no es valido."
            ]);
          }
      
          $decoded=JWTAuth::setToken($request->bearerToken())->getPayload();
          $tipoUsuario=$decoded['tipoUsuario'];
          if ($tipoUsuario=="EMPLEADO"){

            $idPedido=$idPedido ;
            $estado=$estado;
           
            $idEmpleado=$decoded['idEmpleado'];

            $data = DB::select("CALL `prActualizarpedido`(?,?,?);", array($estado,$idPedido,$idEmpleado));
           
            if(empty($data)){
                return response()->json([
                    'estado'=>false,
                    'mensaje'=>"NO da respuesta."
                ]);
            }
            return response()->json([
                'estado'=>$data[0]->estado ? true : false,
                'mensaje'=>$data[0]->mensaje,
            ]); 

          }else{
            return response()->json([
                'estado'=>false,
                'mensaje'=>"Usuario no tiene permisos.",
            ]);   
          } 

        } catch (\Throwable $e) {
          return response()->json([
              'estado'=>false,
              'mensaje'=>$e->getMessage()
          ]);
      }
      
    
    }
    public  function realizarPedido(Request $request){
        try {
          $detalle=$request->input("detallePedido"); 
          $tipoPago=$request->input("tipoPago");
          $estado=$request->input("estado");
          $tokenPago=$request->input("tokenPago");
          $comentario=$request->input("comentario");
          $decoded=JWTAuth::setToken($request->bearerToken())->getPayload();
        
          $tipoUsuario=$decoded['tipoUsuario'];
          $idcliente=$request->input("idCliente");
          $idEmpleado=null;
          if(!($estado=="PEDIDO" || $estado=="PAGADO" || $estado=="ENTREGADO" || $estado=="FINALIZADO")){
             return response()->json([
                  'estado'=>false,
                  'mensaje'=>'El valor del estado pedido no es valido.'
              ]);
          }
          if(mb_strlen($comentario)>200){
            return response()->json([
               "estado"=>false,
               "mensaje"=>"El Comentarios sobrepasa lo establecido."
           ]);
         }
         if(is_null($tokenPago)){
            return response()->json([
               "estado"=>false,
               "mensaje"=>"El tokenPago es requerido."
           ]);
         }
         if(mb_strlen($tipoPago)>45){
            return response()->json([
               "estado"=>false,
               "mensaje"=>"El tipoPago sobrepasa lo establecido."
           ]);
         }
          if(is_null($tipoPago) || $tipoPago==""){
            return response()->json([
                'estado'=>false,
                'mensaje'=>'El Tipo de pago es requerido.'
            ]);
          }

          if($tipoUsuario=="CLIENTE"){
              $idcliente=$decoded['idCliente'];
              $idEmpleado=null;
          }else{
            $idEmpleado=$decoded['idEmpleado'];
              if(is_null($idcliente)){
                 return response()->json([
                      'estado'=>false,
                      'mensaje'=>'Se requiere de cliente.',
                  ]);
              }
              if(!preg_match("/^[0-9]+$/", $idcliente)){
                return response()->json([
                    'estado'=>false,
                    'mensaje'=>'Se requiere de cliente.',
                ]);
              }
  
          }
         
          foreach ($detalle as $key => $value) {
                 
            if((!array_key_exists('idProducto',$detalle[$key] ) || !array_key_exists('cantidad',$detalle[$key])) ){
               return response()->json([
                    "estado"=>false,
                    "mensaje"=>"El Detalle no valido de productos."
                ]);
            }  
            if(!preg_match("/^[0-9]+$/", $detalle[$key]["idProducto"])){
               return response()->json([
                    "estado"=>false,
                    "mensaje"=>"El id del producto debe ser numerico."
                ]);
            }
            if(!preg_match("/^[0-9]+$/", $detalle[$key]["cantidad"])){
               return response()->json([
                    "estado"=>false,
                    "mensaje"=>"El cantidad del producto debe ser numerico."
                ]);
            }
        } 
  
        foreach ($detalle as $key => $value) {
            $cont=0;
            foreach ($detalle as $key => $value2) {
                if($value["idProducto"]==$value2["idProducto"]){
                    $cont=$cont+1;
                }
            }
            if($cont>1){
               return response()->json([
                    'estado'=>false,
                    'mensaje'=>'El id del producto debe ser único por array.',
                ]);
            }            
        }
  
              foreach ($detalle as $key => $value) {
                  $idProducto=$value["idProducto"];
                  $condetalle=null;


                  $data = DB::select("CALL `prconsultarproducto`(?,?,?);", array(null,$idProducto,null));
           
                  if(empty($data)){
                      return response()->json([
                          'estado'=>false,
                          'mensaje'=>"El producto no existe."
                      ]);
                  }

              }
              $idEmpleado=is_null($idEmpleado) ? null : $idEmpleado;
              $estado=$estado  ? 1 :0;
              $comentario=is_null($comentario) || $comentario=="" ? null : $comentario;

              $data = DB::select("CALL `realizarpedido`(?,?,?,?,?,?);", array($idEmpleado,$idcliente,$estado,$tipoPago,$comentario,$tokenPago));
              
              if(empty($data)){
                  return response()->json([
                      'estado'=>false,
                      'mensaje'=>"El producto no existe."
                  ]);
              }
              $pedido=$data[0]->idpedido;
              foreach ($detalle as $key => $value) {
                  $idProducto=$value["idProducto"];
                  $cantidad=$value["cantidad"];
               
                  $data = DB::select("CALL `realizarpedidodetalle`(?,?,?);", array($pedido,$idProducto,$cantidad));
           
                  if(empty($data)){
                      return response()->json([
                          'estado'=>false,
                          'mensaje'=>"El producto no existe."
                      ]);
                  }
              }
             
             return response()->json([
                  'estado'=>true,
                  'mensaje'=>'El pedido se realizo con exito.',
              ]); 
              

        } catch (\Throwable $e) {
         return response()->json([
              'estado'=>false,
              'mensaje'=>$e->getMessage()
          ]);
      }
      }
}
