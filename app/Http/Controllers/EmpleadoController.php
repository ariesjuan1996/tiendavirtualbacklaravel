<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
class EmpleadoController extends Controller
{
    public function verificarDocumentoIdentidad(Request $request){
        try {

            $documento=is_null($request->input("documento")) || $request->input("documento")=="" ? null : $request->input("documento");         
   
            if(!preg_match("/^[0-9]+$/", $documento)){
                return response()->json([
                    "estado"=>false,
                    "mensaje"=>"El documento debe ser numerico."
                ]);
            } 
            if(strlen($documento)!=8) {
                return response()->json([
                    "estado"=>false,
                    "mensaje"=>"El documento debe tener 8 digitos."
                ]);
            }
            $data = DB::select("CALL `verificarDocumentoIdentidad`(?);", array($documento));
 
            if(empty($data)){
                return response()->json([
                    "estado"=>false,
                    "mensaje"=>"No ahy respuesta del servidor.",
                ]);
            }
            return response()->json([
                "estado"=>$data[0]->estado==1 ? true : false,
                "mensaje"=>$data[0]->mensaje,
            ]);

    
        } catch (\Throwable $th) {
           return response()->json([
                'estado'=>false,
                'mensaje'=>$th->getMessage() ]);
        }
    }
}
