<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Validator;
use JWTAuth;
class ProductosController extends Controller
{
    public  function registraProducto(Request $request){
        try {

            $nombreProducto=empty($request->input("nombreProducto"))  ? null :$request->input("nombreProducto");
            $descripcion=empty($request->input("descripcion"))  ? null :$request->input("descripcion");
            $idCategoria=empty($request->input("idCategoria"))  ? null :$request->input("idCategoria");
            $novedad=is_null($request->input("novedad")) || $request->input("novedad")==""  ? null :$request->input("novedad");
            $precioVenta=is_null($request->input("precioVenta")) || $request->input("precioVenta")==""  ? null :$request->input("precioVenta");
            $estado=is_null($request->input("estado")) || $request->input("estado")==""   ? null :$request->input("estado");
            $detalles=empty($request->input("detalles"))  ? null :json_decode($request->input("detalles"));
            $imagenes=empty($request->file("imagen"))  ? null :$request->file("imagen");
            if(is_null($imagenes)){
                return response()->json([
                    "estado"=>false,
                    "mensaje"=>"Nombre del producto es requerido."
                ]);         
            }
            $input  = array('image' => $imagenes);
            $reglas = array('image' => 'mimes:jpeg,png,svg,gif', 'file' => 'size:500');
            $validacion = Validator::make($input,  $reglas);
         
            if ($validacion->fails())
            {
                return response()->json([
                    "estado"=>false,
                    "mensaje"=>"EL archivo debe ser tipo imagen [jpeg,png,svg,gif]."
                ]); 
            }
        
            if(is_null($nombreProducto)){
                return response()->json([
                    "estado"=>false,
                    "mensaje"=>"Nombre del producto es requerido."
                ]);         
            }
            if(mb_strlen($nombreProducto)>150){
                return response()->json([
                    "estado"=>false,
                    "mensaje"=>"El nombreProducto sobrepasa la longitud establecida."
                ]);
            }
            if(is_null($descripcion)){
                return response()->json([
                    "estado"=>false,
                    "mensaje"=>"la Descripcion del producto es requerido."
                ]);         
            }
            if(mb_strlen($descripcion)>300){
                return response()->json([
                    "estado"=>false,
                    "mensaje"=>"La descripcion sobrepasa la longitud establecida."
                ]);
            }
            if(is_null($idCategoria)){
                return response()->json([
                    "estado"=>false,
                    "mensaje"=>"El idcategoria del producto es requerido."
                ]);         
            }
            if(!preg_match("/^[0-9]+$/", $idCategoria)){
                return response()->json([
                    "estado"=>false,
                    "mensaje"=>"El atributo idcategoria es numerico."
                ]);
            }
           
            if(is_null($novedad)){
                return response()->json([
                    "estado"=>false,
                    "mensaje"=>"El atributo novedad es requerido."
                ]);         
            }
            if(!preg_match("/^[0-9]+$/", $novedad)){
                return response()->json([
                    "estado"=>false,
                    "mensaje"=>"El atributo novedad es numerico."
                ]);
            }
            if(is_null($precioVenta)){
                return response()->json([
                    "estado"=>false,
                    "mensaje"=>"El atributo precio de venta es requerido."
                ]);         
            }
            if(!preg_match("/^[0-9]+(\.[0-9]{1,2})?$/", $precioVenta)){
                return response()->json([
                    "estado"=>false,
                    "mensaje"=>"El atributo precio de venta de ser numerico."
                ]);
            }
            if(is_null($estado)){
                return response()->json([
                    "estado"=>false,
                    "mensaje"=>"El estado del producto es requerido."
                ]);         
            }
            if( !(mb_strtoupper($estado)==mb_strtoupper("TRUE") || mb_strtoupper($estado)==mb_strtoupper("FALSE")) ){
                return response()->json([
                    "estado"=>false,
                    "mensaje"=>"El estado debe ser true or false."
                ]);
            }
            
            if ( is_null($detalles)){
                return response()->json([
                    "estado"=>false,
                    "mensaje"=>"Formato del json no valido."
                ]);
            }else{
             
                foreach ($detalles as $key => $value) {
                   
                    if((!array_key_exists('titulo',$detalles[$key] ) || !array_key_exists('descripcion',$detalles[$key])) ){
                        return response()->json([
                            "estado"=>false,
                            "mensaje"=>"El formato del detalle es invalido."
                        ]);
                    }  
                }  
    
            }  
            $data = DB::select("CALL `verificarnombreproducto`(?);", array($nombreProducto));
       
            if(empty($data)){
                return response()->json([
                    "estado"=>false,
                    "mensaje"=>"No ahy respuesta del servidor.",
                ]);
            }
            $estadoFlag=$data[0]->estado ? true : false;
            if($estadoFlag){
                return response()->json([
                    "estado"=>false,
                    "mensaje"=>$data[0]->mensaje,
                ]);
            }
           
            $milliseconds = round(microtime(true) * 1000);
            $ruta = "img/" .$milliseconds.".". $imagenes->getClientOriginalExtension();
            $valor=$imagenes->move(public_path().'/img/',$ruta);
            $input  = array('image' => $valor);
            $reglas = array('image' => 'mimes:jpeg,png,svg,gif', 'file' => 'size:500');
            $validacion = Validator::make($input,  $reglas);
         
            if ($validacion->fails())
            {
                return response()->json([
                    "estado"=>false,
                    "mensaje"=>"Erroor al importar archivo."
                ]); 
            }
            $res=JWTAuth::setToken($request->bearerToken())->getPayload();
      
            $idEmpleado=$res['idEmpleado'];
            $Datadetalles=json_encode($detalles);
            $data = DB::select("CALL `prregistrarproducto`(?,?,?,?,?,?,?,?);", array($nombreProducto,$descripcion,$idCategoria,$novedad,$precioVenta,$ruta,$Datadetalles,$idEmpleado));
            
            if(empty($data)){
                return response()->json([
                    "estado"=>false,
                    "mensaje"=>"No Hubo respuesta del servidor.",
                ]);
            }
            return response()->json([
                "estado"=>$data[0]->estado ? true : false,
                'mensaje'=>$data[0]->mensaje
            ]);          


        } catch (\Throwable $th) {
            //throw $th;
            return response()->json([
                'estado'=>false,
                'mensaje'=>$th->getMessage()
            ]);
        }
    
    }
    public function  verificarNombreProducto(Request $request){
        $nombreProducto=empty($request->input("nombreProducto"))  ? null :$request->input("nombreProducto");
        if(is_null($nombreProducto) ){
            return response()->json([
                "estado"=>false,
                "mensaje"=>"El nombreProducto de la producto es requerido."
            ]);

        }
        if(mb_strlen($nombreProducto)>150){
            return response()->json([
                "estado"=>false,
                "mensaje"=>"El atributo nombre de producto sobrepasa la longitud establecida."
            ]);
        }
        try {
           
        $data = DB::select("CALL `verificarnombreproducto`(?);", array($nombreProducto));
       
        if(empty($data)){
            return response()->json([
                "estado"=>false,
                "mensaje"=>"No se encontraron datos.",
            ]);
        }
        return response()->json([
        "estado"=>$data[0]->estado ? true : false,
        'mensaje'=>$data[0]->mensaje
        ]);

     } catch (\Throwable $th) {
        return response()->json([
            'estado'=>false,
            'mensaje'=>$th->getMessage()
        ]);

     }
    }
    public function listarDetalleProducto(Request $request){
        $idProducto=empty($request->input("idProducto"))  ? null :$request->input("idProducto");
        
        if(!preg_match("/^[0-9]+$/", $idProducto)){
            return response()->json([
                "estado"=>false,
                "mensaje"=>"El id de la producto debe ser numerico."
            ]);
        }           

        if(!is_null($idProducto) ){
            if(!preg_match("/^[0-9]+$/", $idProducto)){
                return response()->json([
                    "estado"=>false,
                    "detalles"=>"El id de la producto debe ser numerico."
                ]);
            }           
        }
        try {
            $idProducto=is_null($idProducto) ? null:$idProducto;

            $data = DB::select("CALL `prconsultardetalleproducto`(?);", array($idProducto));
            $dataResponse=[];
     
            if(empty($data)){
                return response()->json([
                    'estado'=>false,
                    'mensaje'=>"No se encontraron datos."
                ]);
            }else{
                foreach ($data as  $value) {
                    array_push($dataResponse,[
                        'detalles'=>json_decode($value->detalleproducto),
                    ]);
                }
                return response()->json([
                    'estado'=>true,
                    'data'=>$dataResponse
                ]);
            }    
               

        } catch (PDOException $e) {

        return response()->json([
            'estado'=>false,
            'mensaje'=>$e->getMessage()
        ]);
        }
    }
    public function filtrarProductos(Request $request){
        $idCategoria=is_null($request->input("idCategoria")) || $request->input("idCategoria")==""  ? null :$request->input("idCategoria");
        $novedad=is_null($request->input("novedad")) || $request->input("novedad")==""  ? null :$request->input("novedad");
        $idProducto=is_null($request->input("idProducto")) || $request->input("idProducto")==""  ? null :$request->input("idProducto");
           
        if(!is_null($idCategoria) ){
            if(!preg_match("/^[0-9]+$/", $idCategoria)){
                return response()->json([
                    "estado"=>false,
                    "mensaje"=>"El id de la categoria debe ser numerico."
                ]);
            }           
        }
        if(!is_null($novedad) ){
            if(!preg_match("/^[0-9]+$/", $novedad)){
                return response()->json([
                    "estado"=>false,
                    "mensaje"=>"La novedad  debe ser numerico."
                ]);
            }           
        }
        if(!is_null($idProducto) ){
            if(!preg_match("/^[0-9]+$/", $idProducto)){
                return response()->json([
                    "estado"=>false,
                    "mensaje"=>"El id de la producto debe ser numerico."
                ]);
            }           
        }
        if(!is_null($idProducto) ){
            if(!preg_match("/^[0-9]+$/", $idProducto)){
                return response()->json([
                    "estado"=>false,
                    "mensaje"=>"El id de la producto debe ser numerico."
                ]);
            }           
        }
        try {

            $novedad=is_null($novedad) ? null : $novedad;
            $idProducto=is_null($idProducto) ? null:$idProducto;
            $idCategoria=is_null($idCategoria) ? null : $idCategoria;

            $data = DB::select("CALL `prconsultarproducto`(?,?,?);", array($idCategoria,$idProducto,$novedad));
            $dataResponse=[];
     
            if(empty($data)){
                return response()->json([
                    'estado'=>false,
                    'mensaje'=>"No se encontraron datos."
                ]);
            }else{
                foreach ($data as  $value) {
                    array_push($dataResponse,[
                        'id'=>$value->id,
                        'nombre'=>$value->nombreproducto,
                        'descripcion'=>$value->descripcion, 
                        'idCategoria'=>$value->idcategoria,
                        'novedad'=>$value->novedad, 
                        'precioVenta'=>$value->precioventa,
                        'estado'=>$value->estado, 
                        'detalles'=>json_decode($value->detalles), 
                        'img'=>$request->root()."/".$value->img,
                        'fechaCreacion'=>$value->fechacreacion,
                        'fechaUltimaModificacion'=>$value->fechaultimamodificacion,
                        'idempleadoUltimaModificacion'=>$value->idempleado,
                        'empleadoUltimoNombre'=>$value->nombreultimoempleado,
                        'empleadoUltimoApellidoPaterno'=>$value->apellidopaternoultimoempleado,
                        'empleadoUltimoApellidoMaterno'=>$value->apellidomaternoultimoempleado
                    ]);
                }
                return response()->json([
                    'estado'=>true,
                    'data'=>$dataResponse
                ]);
            }    
               

        } catch (\Throwable $th) {

        return response()->json([
            'estado'=>false,
            'mensaje'=>$th->getMessage()
        ]);
        }
    }
    public function filtraCategoriasPorProducto(Request $request){
        
        try {
            $data = DB::select("CALL `prlistarcategorias`();", array());
            $datos=[];
            if(empty($data)){
                return response()->json([
                    "estado"=>false,
                    "mensaje"=>"No se encontraron datos.",
                ]);
            }
           
            foreach ($data as  $value) {
                # code...
                $dataProductos = DB::select("CALL `prconsultarproducto`(?,?,?);", array($value->id,null,null));
                
                $dataResponseProductos=[];
                if(empty($dataProductos)){
                    $dataResponseProductos=[];
                }else{
                    foreach ($dataProductos as  $valueProducto) {
                    if($valueProducto->estado){
                        array_push($dataResponseProductos,[
                            'id'=>$valueProducto->id,
                            'nombre'=>$valueProducto->nombreproducto,
                            'descripcion'=>$valueProducto->descripcion, 
                            'idCategoria'=>$valueProducto->idcategoria,
                            'novedad'=>$valueProducto->novedad, 
                            'precioVenta'=>$valueProducto->precioventa,
                            'estado'=>$valueProducto->estado, 
                            'detalles'=>json_decode($valueProducto->detalles), 
                            'img'=>$request->root()."/".$valueProducto->img,
                            'fechaCreacion'=>$valueProducto->fechacreacion,
                            'fechaUltimaModificacion'=>$valueProducto->fechaultimamodificacion,
                            'idempleadoUltimaModificacion'=>$valueProducto->idempleado,
                            'empleadoUltimoNombre'=>$valueProducto->nombreultimoempleado,
                            'empleadoUltimoApellidoPaterno'=>$valueProducto->apellidopaternoultimoempleado,
                            'empleadoUltimoApellidoMaterno'=>$valueProducto->apellidomaternoultimoempleado
                        ]);
                    }

                    }
         
                }   

                array_push($datos,[
                    'id'=>$value->id,
                    'nombre'=>$value->nombre,
                    'descripcion'=>$value->descripcion, 
                    'img'=>$request->root()."/".$value->img, 
                    'productos'=>$dataResponseProductos
                ]);
            }
            
            return response()->json([
                'estado'=>true,
                'data'=>$datos
            ]);
               

        } catch (\Throwable $th) {

        return response()->json([
            'estado'=>false,
            'mensaje'=>$th->getMessage()
        ]);
        }
    }
    public function categorias(Request $request){
        
        try {
            $data = DB::select("CALL `prlistarcategorias`();", array());
            $datos=[];
            if(empty($data)){
                return response()->json([
                    "estado"=>false,
                    "mensaje"=>"No se encontraron datos.",
                ]);
            }
            foreach ($data as  $value) {
                # code...
                array_push($datos,[
                    'id'=>$value->id,
                    'nombre'=>$value->nombre,
                    'descripcion'=>$value->descripcion, 
                    'img'=>$request->root()."/".$value->img, 
                ]);
            }
        return response()->json([
            "estado"=>true,
            'data'=>$datos
        ]);            
        } catch (\Throwable $th) {
            return response()->json([
                "estado"=>false,
                'mensaje'=>$th->getMessage()
            ]);
        }
    }
    public function indexPrincipal(Request $request){
        return view('pagePrincipal');
    }
    public function productos(Request $request){
        $data = DB::select("CALL `prlistarcategorias`();", array());
        $datos=[];

        foreach ($data as  $value) {
            $dataProductos = DB::select("CALL `prconsultarproducto`(?,?,?);", array($value->id,null,null));
            
            $dataResponseProductos=[];
            if(empty($dataProductos)){
                $dataResponseProductos=[];
            }else{
                foreach ($dataProductos as  $valueProducto) {
                if($valueProducto->estado){
                    $descripcionProductoMax100 = substr($valueProducto->descripcion,0,100);
             
                    array_push($dataResponseProductos,[
                        'id'=>$valueProducto->id,
                        'nombre'=>$valueProducto->nombreproducto,
                        'descripcion'=>$valueProducto->descripcion, 
                        'descripcionProductoMax'=>$descripcionProductoMax100, 
                        'idCategoria'=>$valueProducto->idcategoria,
                        'novedad'=>$valueProducto->novedad, 
                        'precioVenta'=>$valueProducto->precioventa,
                        'estado'=>$valueProducto->estado, 
                        'detalles'=>json_decode($valueProducto->detalles), 
                        'img'=>$request->root()."/".$valueProducto->img,
                        'fechaCreacion'=>$valueProducto->fechacreacion,
                        'fechaUltimaModificacion'=>$valueProducto->fechaultimamodificacion,
                        'idempleadoUltimaModificacion'=>$valueProducto->idempleado,
                        'empleadoUltimoNombre'=>$valueProducto->nombreultimoempleado,
                        'empleadoUltimoApellidoPaterno'=>$valueProducto->apellidopaternoultimoempleado,
                        'empleadoUltimoApellidoMaterno'=>$valueProducto->apellidomaternoultimoempleado
                    ]);
                }

                }
     
            }   
            $descripcionMax100 = substr($value->descripcion,0,10);
             
            array_push($datos,[
                'id'=>$value->id,
                'nombre'=>$value->nombre,
                'descripcion'=>$value->descripcion, 
                'descripcionMax100'=>$descripcionMax100, 
                'img'=>$request->root()."/".$value->img, 
                'productos'=>$dataResponseProductos
            ]);
        }
        $categorias=$datos;
        return view('productos',compact('categorias'));
    }
    public function indexCarrito(Request $request){
        return view('carrito');
    }
    public function finalizar(Request $request){
        return view('finalizar');
    }
}
