<section id="tab" class="tabContainer">
    <div class="container">
        
        @include('header/categorias')

        <div id="panels">
            <?php $posCategoria=0 ?>
            @foreach ($categorias as $index => $value)
            <div id="opt{{$value['id']}}" class="tabPanel tabPanelProductos "
            data-index-categoria="{{$posCategoria}}">
                <h1 class="title-panel">{{$value["nombre"]}}</h1>
                
                <?php $posProducto=0 ?>
                @foreach ($value["productos"]  as $indexKey => $valueProducto)
                <div class="products col" id="sec-products"  
                data-index-producto="{{$posProducto}}" 
                data-index-categoria="{{$posCategoria}}"  
                >
                    <div class="cont row end">
                        <div class="card-product">
                            <div class="container">
                                <div class="img-card">
                                    <img  data-index-producto="{{$posProducto}}" 
                                    data-index-categoria="{{$posCategoria}}"  class="mostrarInfo pointer" src="{{$valueProducto['img']}}">
                                </div>
                                <div class="body-card">
                                    <div class="item-content">
                                        <a class="name mostrarInfo">{{$valueProducto['nombre']}}</a>
                                        <p class="descrip-item">{{$valueProducto["descripcionProductoMax"]}}
                                        </p>
                                    </div>
                                    <div class="item-pie row space-between">
                                        <p class="price center">{{$valueProducto["precioVenta"]}}</p>
                                        <div class="col itemproducto">
                                            <a class="agregar redireccionar" href="#" data-id="{{$valueProducto['id']}}"  > Agregar </a>
                                            <div class="" id="inputNum">
                                                <input 
                                                id="precioVenta{{$valueProducto['id']}}"
                                                data-index-producto="{{$posProducto}}" 
                                                data-index-categoria="{{$posCategoria}}"
                                                class="valuecantidad" 
                                                data-precio="{{$valueProducto['precioVenta']}}" 
                                                data-id="{{$valueProducto['id']}}" 
                                                data-nombre="{{$valueProducto['nombre']}}"  
                                                data-img="{{$valueProducto['img']}}"
                                                type="number" value="1" min="1" 
                                                onblur='anadirProductoBlur(this)'/>
                                                
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="modal-desc">
                                <div class="desc-card animate__animated ">
                                    <div class="close-desc">
                                        <span class="close-btn">&times;</span>
                                    </div>
                                    <div class="col-desc">
                                        <div class="img-desc">
                                            <img  src="{{$valueProducto['img']}}" alt="">
                                        </div>
                                        <div class="row-desc">
                                            <div class="body-desc">
                                                <div class="text-name">
                                                    <p>{{$valueProducto["nombre"]}}</p>
                                                </div>
                                                <div class="text-desc">
                                                    <p>{{$valueProducto["descripcion"]}}

                                                    </p>
                                                </div>
                                                <div class="precio-desc">
                                                    <p class="price">{{$valueProducto["precioVenta"]}}</p>
                                                </div>
                                            </div>
                                            <div class="footer-desc row">
                                                <div  id="inputNum">
                                                    <input 
                                                    id="precioVenta"
                                                    data-index-producto="{{$posProducto}}" 
                                                    data-index-categoria="{{$posCategoria}}"  
                                                    class="classinputdetalle" 
                                                    data-precio="{{$valueProducto['precioVenta']}}" 
                                                    data-nombre="{{$valueProducto['nombre']}}"
                                                    data-img="{{$valueProducto['img']}}"
                                                    data-id="{{$valueProducto['id']}}" 
                                                     type="number" value="1" min="1" 
                                                    >
                                                </div>
                                                <button 
                                                    class="redireccionar anadirProducto" 
                                                    data-index-producto="{{$posProducto}}" 
                                                    data-index-categoria="{{$posCategoria}}"
                                                    data-precio="{{$valueProducto['precioVenta']}}" 
                                                    data-nombre="{{$valueProducto['nombre']}}" 
                                                    data-img="{{$valueProducto['img']}}"
                                                    data-id="{{$valueProducto['id']}}" 
                                                 >Agregar S/{{ $valueProducto["precioVenta"]}}</button>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <?php $posProducto=$posProducto+1 ?>
                @endforeach    

            </div>
            <?php $posCategoria=$posCategoria+1 ?>
            @endforeach


  

        </div>
    </div>

</section>
