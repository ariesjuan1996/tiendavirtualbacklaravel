<section id="wrapper">
    <div id="slider-area" class="owl-carousel">
        <div class="item">
            <img src="images/Home2.jpg" alt="">
            <div class="content-center">
                <p>Pide cuando quieras,<br> come cuando quieras.</p>
                <a href="#comunicado" class="linkCom redireccionar ">Ver Productos</a>
            </div>

        </div>
        <div class="item">
            <img src="images/Home3.jpg" alt="">
            <div class="content-center">
                <p>Pide cuando quieras,<br> come cuando quieras.</p>
                <a href="#comunicado" class="linkCom redireccionar ">Ver Productos</a>
            </div>
        </div>
        <div class="item">
            <img src="images/Home1.jpg" alt="">
            <div class="content-center">
                <p>Pide cuando quieras,<br> come cuando quieras.</p>
                <a href="#comunicado" class="linkCom redireccionar ">Ver Productos</a>
            </div>
        </div>
        <div class="item">
            <img src="images/Home4.jpg" alt="">
            <div class="content-center">
                <p>Pide cuando quieras,<br> come cuando quieras.</p>
                <a href="#comunicado" class="linkCom redireccionar ">Ver Productos</a>
            </div>
        </div>
        <div class="item">
            <img src="images/Home5.jpg" alt="">
            <div class="content-center">
                <p>Pide cuando quieras,<br> come cuando quieras.</p>
                <a href="#comunicado" class="linkCom redireccionar ">Ver Productos</a>
            </div>
        </div>

    </div>
</section>