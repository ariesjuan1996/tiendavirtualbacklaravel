<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no ,initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="{{ URL::asset('css/css.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('css/jquery.nice-number.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.0.0/animate.min.css" />
    <link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    <link rel="stylesheet" href="{{ URL::asset('css/owl.carousel.min.css') }}">
    <link href="{{ URL::asset('css/owl.theme.default.min.css') }}" rel="stylesheet" type="text/css" >
    <style>
        .activebtn{
            color: red !important;
        }
    </style>
</head>

<body>

    @yield('headerProducto')

    @yield('containerProducto')

    @yield('script')
    <!--SCRIPTS-->

    
</body>

</html>