<footer>
    <section id="footer">
        <div class="container">
            <div class="row space-between">
                <div class="msj card-footer col center">
                    <a class="content-msj sugg">
                        <p>Qué más te gustaría encontrar en</p>
                        <img src="images/logo_header.jpg" alt="">
                    </a>
                </div>
                <div class="enlaces-rap card-footer center">
                    <h3 class="title-footer">Frozen Market</h3>
                    <ul>
                        <li class="enlace">
                            <a class="enlacesa" href="">
                                Quiénes Somos
                            </a>
                        </li>
                        <li class="enlace">
                            <a class="enlacesa" href="">
                                Términos y condiciones
                            </a>
                        </li>
                        <li class="enlace">
                            <a class="enlacesa" href="">
                                Políticas de privacidad
                            </a>
                        </li>
                        <li class="enlace">
                            <a class="enlacesa" href="">
                                Libro de reclamaciones
                            </a>
                        </li>
                    </ul>

                </div>
                <div class="contacto card-footer center">
                    <h3 class="title-footer">Contáctanos</h3>
                    <ul>
                        <li class=""><i class="fa fa-envelope-o"></i> info@frozenmarket.pe</li>
                        <li class=""><i class="fa fa-whatsapp"></i> 987654321</li>
                    </ul>
                </div>
                <div class="social card-footer center">
                    <h3 class="title-footer">Redes Sociales</h3>
                    <ul>
                        <li><a class="facebook enlacesa"><i class="fa fa-facebook"></i> </a></li>
                        <li><a class="instagram enlacesa"><i class="fa fa-instagram"></i> </a></li>
                    </ul>
                </div>

            </div>
        </div>
    </section>

    <section id="copyright">

        <div class="copyright">
            Copyright © 2020 Todos los derechos reservados, by <a href="https://ideasweb.com.pe"> Ideas Web</a>
        </div>
    </section>
</footer>

<div id="modal-sugerencias" class="">
    <div class="form-sugerencias absolute-center animate__animated animate__slideInUp">
        <div class="content-sug">
            <div class="close-modal">
                <span class="btn-close">&times;</span>
            </div>
            <div class="header-form-sug">
                <img src="images/logo_header.jpg" alt="">
            </div>
            <div class="form-sug">
                <h3 class="title-form">¿Qué más te gustaría encontrar?</h3>
                <form action="">
                    <label for="" class="neg">Dirección de correo electrónico</label>
                    <input type="email" placeholder="example@email.com" required>
                    <textarea name="" id="" placeholder="Detalle aquí"></textarea>
                    <a href="" class="redireccionar">Enviar</a>
                </form>
            </div>
        </div>
    </div>
</div>