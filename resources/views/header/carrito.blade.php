<div id="modal-carrito-header">
    <div id="carrito-header" class="form-box-lo animate__animated animate__slideInUp">
        <div class="close-carrito">
            <span class="btn-close-carrito">&times;</span>
        </div>
        <ul id="listProductoCarrito">

        </ul>
        <div class="resumen">
            <div class="line-products row space-between">
                <p><span class="num-products" id="cantidadProducto">0</span>artículos</p>
                <p class="neg price" id="precioSubTotal">0.00</p>
            </div>
            <div class="line-products row space-between">
                <p>Transporte</p>
                <p class="neg ">Gratis</p>
            </div>
            <div class="card-resumen">
                <div class="body-card-res row space-between">
                    <p>Total</p>
                    <p class="neg price" id="precioTotal">0.00</p>
                </div>
            </div>
            <div class="line-products center flex">
                <a class="btn-cart-header" href="carrito">Ir a carrito</a>
            </div>
        </div>

    </div>
</div>