<div id="modal-login">
    <div id="form-box" class="form-box-lo animate__animated animate__slideInUp">
        <div class="close-carrito">
            <span class="btn-close-carrito">&times;</span>
        </div>

        <div class="btn-box">
            <button id="btn-login" type="button" class="toggle-btn activo-btn">Iniciar Sesión</button>
            <button id="btn-register" type="button" class="toggle-btn">Registrarse</button>
        </div>
        <form id="login" action="" class="input-group">
            <input autocomplete="off" type="email" class="input-field" name="email" id="email" placeholder="Email"  required>
            <input autocomplete="off" type="password" class="input-field" name="contrasenia" id="contrasenia" placeholder="Password" required>
            <a href="" class="link-comprar">¿Olvidó su Contraseña?</a>
            <button type="submit" class="btn-submit redireccionar">Inicia Sessión</button>
        </form>

        <form id="register" action="" class="input-group">
            <input 
            type="text" 
            autocomplete="off"
            class="input-field" 
            name="nombres" 
            id="nombresRegistro" 
            placeholder="Nombres" 
            maxlength="250"
             required/>
            <input 
            type="text" 
            autocomplete="off"
            class="input-field"
            name="apellidos" 
            id="apellidosRegistro"  
            placeholder="Apellidos" required maxlength="250" />
            <input 
            autocomplete="off"
            type="email" 
            class="input-field" 
            name="email" 
            id="emailRegistro" 
            placeholder="Email" 
            required 
            maxlength="250"/>
            <input type="password" class="input-field" name="contrasenia" 
            id="contraseniaRegistro" placeholder="Password" required min="6" />

            <div class="box-row uno">
                <input type="checkbox" name="" id="">
                <span>Recibir Notificaciones</span>
            </div>
            <div class="box-row dos">
                <input type="checkbox" name="" id="">
                <span>Acepto los términos y condiciones así como la política de privacidad.</span>
            </div>

            <button type="submit" class="registrarse redireccionar" >Registrarse</button>
        </form>

    </div>
</div>