<section id="carrito" style="margin-top: 50px;">
    <div class="container">
        <div class="carrito-box row">
            <div class="col">
                <div class="detalles">
                    <div class="header-box">
                        <h1 class="title-header">Carrito</h1>
                    </div>
                    <div class="body-box">
                        <ul id="detallesCarro">
                            
                        </ul>
                    </div>
                </div>
                <a href="/productos" class="link-comprar">Seguir Comprando</a>
            </div>

            <div class="resumen">
                <div class="resumen-total col">
                    <div class="line-products row space-between">
                        <p><span class="num-products" id="cantidad">0</span>articulos</p>
                        <p class="neg price" id="subTotal">0</p>
                    </div>
                    <div class="line-products row space-between">
                        <p>Transporte</p>
                        <p class="neg ">Gratis</p>
                    </div>
                </div>
                <div class="card-resumen">
                    <div class="body-card-res row space-between">
                        <p class="neg"  >Total</p>
                        <p class="neg price" id="total">0.00</p>
                    </div>
                </div>
                <div class="line-products center flex" >
                    <a id="direccionarpagar" href="finalizar" class="btn-cart" >Ir a Pagar</a>
                </div>
            </div>
        </div>
    </div>
</section>
