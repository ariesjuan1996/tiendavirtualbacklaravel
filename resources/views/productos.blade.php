@extends('productosIndex')
@section('headerProducto')
    @include('headerProducto')
@stop
@section('containerProducto')
    @include('containerProducto')
@stop
@section('script')
    <script src="{{ URL::asset('js/query/api.js') }}"></script>
    <script src="{{ URL::asset('js/query/productosproducto.js') }}"></script>
    <script src="{{ URL::asset('js/jquery.js') }}"></script>
    
    <script src="{{ URL::asset('js/owl.carousel.min.js') }}"></script>
    <script src="{{ URL::asset('js/app.js') }}"></script>
    <script src="{{ URL::asset('js/jquery.nice-number.min.js') }}"></script>
    <script>
        $('#slider-options').owlCarousel({
            items: 5,
            autoplayTimeout: 3000,
            responsiveClass: true,
            autoplaySpeed: 1000,
            nav: true,
            responsive: {
                0: {
                    items: 3,
                    nav: false
                },
                600: {
                    items: 3,
                    nav: false,
                },
                1000: {
                    items: 5,
                    nav: false
                }
            }
        });
    </script>
    <script>
        cargarEventos_Prod();

        function cargarEventos_Prod() {
            productos.addEventListener('click', mostrarModalDescr);
            productos.addEventListener('click', mostrarInputNumber);
        }

        function mostrarModalDescr(e) {
            //alert(e);
            console.log("event",e.target.parentElement.parentElement.parentElement.lastElementChild);
            if (e.target.classList.contains('mostrarInfo')) {
                // console.log(':D');
                var elements = document.querySelectorAll('.itemproducto');
                let indexProducto=parseInt(e.target.dataset.indexProducto) ;
                let indexCategoria=parseInt(e.target.dataset.indexCategoria);
                input=document.getElementsByClassName("valuecantidad")[indexProducto+indexCategoria];
                inputDetalle=document.getElementsByClassName("classinputdetalle")[(parseInt(indexCategoria)+parseInt(indexProducto))];
                //console.log("indexProducto",indexProducto);
                //console.log("input",input);
                //console.log("indexCategoria",indexCategoria);
                inputDetalle.value=input.value
                const modal = e.target.parentElement.parentElement.parentElement.lastElementChild;

                modal.classList.add('mostrar-list');

                modal.lastElementChild.classList.add('animate__slideInUp');
                body.style.overflow = "hidden";

                $(".close-btn").click(function() {
                    let index= e;
                   
                    modal.classList.remove('mostrar-list');
                    
                    body.style.overflow = "visible";
                    // modal.lastElementChild.classList.remove('animate__slideInUp');
                    

                });
            }

        }

        function mostrarInputNumber(e) {
            if (e.target.classList.contains('agregar')) {
                const btnAgregar = e.target.parentElement;
                e.target.classList.add('ocultar');
                btnAgregar.lastElementChild.classList.add('mostrar-input');

                /* class agregar,               
                 const btnAgregar = e.target.parentElement;
                e.target.classList.add('ocultar');
                btnAgregar.lastElementChild.classList.add('mostrar-input');*/
            }
        }
    </script>

    <script>
        $(function() {
            $('input[type="number"]').niceNumber();
        });
        $('.classinputdetalle').on("blur",(e)=>{
            let cantidad=parseInt($(e.target).val());
            if(cantidad<1){
                cantidad=1;
                $(e.target).val(1);
              }
            
        });
    </script>
    <script src="{{ URL::asset('js/query/seguridad.js') }}"></script>
    <script src="{{ URL::asset('js/query/carrito.js') }}"></script>
@stop
