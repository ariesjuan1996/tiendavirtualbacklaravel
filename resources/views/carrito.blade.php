@extends('productosIndex')
@section('headerProducto')
    @include('headerProducto')
@stop
@section('containerProducto')
    @include('detallecarrito')
@stop
@section('script')
    <!--SCRIPTS-->
    <script src="js/jquery.js"></script>
    <script src="{{ URL::asset('js/query/api.js') }}"></script>
    <script src="js/app.js"></script>
    <script src="js/jquery.nice-number.min.js"></script>
    <script src="{{ URL::asset('js/query/seguridad.js') }}"></script>
    <script src="{{ URL::asset('js/query/carrito.js') }}"></script>
    <script type="text/javascript">
        window.addEventListener("scroll", function() {
            var header = document.querySelector("header");
            //header.classList.toggle("sticky", window.scrollY > 0);

            if (window.scrollY > 15 && screen.width < 1100) {
                // alert(':D');

                                $('#logo').css({
                                    position: 'relative',
                                    top: '25px',
                                    left: '0'
                                });
                                
                                $('.row-header').css({
                                    position: 'relative',
                                    top: '-25px'
                                });
                                $('.search-box').css({
                                    background: 'none',
                                    width: '90%'
                                });
                                
                                $('.search-txt').css({
                                    display: 'none'
                                });
                                $('#toggle-box-search').css({
                                    display: 'none'
                                });
                                $('.toggle-btn-nav').css({
                                    display: 'block'
                                });

            }

        })

    </script>


    <script>
        window.addEventListener("load", function(){
            cargarProductosSeleccionado();
        });
        $(function () {
            $('input[type="number"]').niceNumber();
        });
    function  eliminar(id){
        var listProductosLocal=localStorage.getItem("productoList");
        
        if(listProductosLocal!=null){
            let productosSinEliminar=[];
            let productoParse=JSON.parse(listProductosLocal);
            for (let index = 0; index < productoParse.length; index++) {
                if(productoParse[index].id!=id){
                    productosSinEliminar.push(productoParse[index]);
                }
            }
         
            localStorage.setItem("productoList",JSON.stringify(productosSinEliminar));
            cargarProductosSeleccionado();
            cargarDataCarrito();
        }
    }
    function  cargarProductosSeleccionado() {
        var listProductosLocal=localStorage.getItem("productoList");
        let domIdProducto=document.getElementById("detallesCarro");
        let domCantidadProducto=document.getElementById("cantidad");
        let domSubTotal=document.getElementById("subTotal");
        let domTotal=document.getElementById("total");
        //let domCantidadProducto=document.getElementById("cantidadProducto");
        if(listProductosLocal!=null){
            var productoParse=JSON.parse(listProductosLocal);
            let htmlProductoDetalles="";
            let cantidad=0;
            let subtotal=0;
            for (let index = 0; index < productoParse.length; index++) {
                cantidad=cantidad+parseInt(productoParse[index].cantidad);
                subtotal=subtotal+parseInt(productoParse[index].cantidad)*parseFloat(productoParse[index].precioVenta);
                htmlProductoDetalles=htmlProductoDetalles+`<li class="item-cart">
                                    <div class="item-product">
                                        <div class="img-item">
                                            <img src="${productoParse[index].img}" alt="">
                                        </div>
                                        <div class="txt-item">
                                            <p class="name">${productoParse[index].nombre}</p>
                                            <div class="price-box">
                                                <p class="det-mov">Precio Unitario</p>
                                                <p class="price">${productoParse[index].precioVenta}</p>
                                            </div>
                                        </div>
                                        <div class="options-item">
                                            <div class="mostrar-input" id="inputNum">
                                                <div class="nice-number">
                                                    <button type="button"  onClick="agregarMenos(${productoParse[index].id},${productoParse[index].precioVenta})" data-object='${JSON.stringify(productoParse[index])}'>-</button>
                                                    <input id="producto${productoParse[index].id}"  class="valuecantidad" type="number" 
                                                    value="${productoParse[index].cantidad}" min="1" 
                                                     data-object='${JSON.stringify(productoParse[index])}' 
                                                     onblur="actualizarCantidad(${productoParse[index].id})"
                                                    data-nice-number-initialized="true" style="width: 2ch;" />
                                                    <button type="button" onClick="agregarMas(${productoParse[index].id},${productoParse[index].precioVenta})" data-object='${JSON.stringify(productoParse[index])}'>+</button></div>
                                            </div>
                                            <div class="total-item">
                                                <p class="det-mov">Precio Total</p>
                                                <p class="price total classTotal">${parseInt(productoParse[index].cantidad)*parseFloat(productoParse[index].precioVenta)}</p>
                                            </div>
                                            <a class="remove-product" onclick="eliminar(${productoParse[index].id})">
                                                <i class="fa fa-trash"></i>
                                            </a>
                                        </div>
                                    </div>
                                </li>`;
            }
            domIdProducto.innerHTML=htmlProductoDetalles;
            domCantidadProducto.innerHTML=cantidad;
            console.log("domCantidadProducto",domCantidadProducto);
            if(productoParse.length==0){
                //alert("primero elijar almenos un producto.");
                $('#direccionarpagar').attr('href', "#");
            }
            domSubTotal.innerHTML=subtotal;
            domTotal.innerHTML=subtotal;
        }else{
            $('#direccionarpagar').attr('href', "#");
        }
    }
    function agregarMas(idproducto,precioventa) {
        let cantidad=$("#producto"+idproducto).val();
        let cantidadAumento=parseInt(cantidad)+1;
        $("#producto"+idproducto).val(cantidadAumento);
        console.log($("#producto"+idproducto).data("object"));
        let objecto=$("#producto"+idproducto).data("object");
        $('.classTotal').html(parseFloat(objecto.precioVenta)*cantidadAumento);
        anadirProducto(cantidadAumento,parseFloat(objecto.precioVenta),objecto.id,objecto.nombre,objecto.img);
        cargarProductosSeleccionado();
    }
    function agregarMenos(idproducto,precioventa) {
        let cantidad=$("#producto"+idproducto).val();
        let cantidadAumento=parseInt(cantidad)-1;
        if(cantidadAumento<1){
            cantidadAumento=1;
        }
            $("#producto"+idproducto).val(cantidadAumento);
            let objecto=$("#producto"+idproducto).data("object");
            $('.classTotal').html(parseFloat(objecto.precioVenta)*cantidadAumento);
            anadirProducto(cantidadAumento,parseFloat(objecto.precioVenta),objecto.id,objecto.nombre,objecto.img);    
            cargarProductosSeleccionado();
    }
    function  actualizarCantidad(idproducto) {
        let cantidad= parseInt($("#producto"+idproducto).val()=="" ? 0 : $("#producto"+idproducto).val());

        let objecto= $("#producto"+idproducto).data("object");
        if(cantidad<1){
            $("#producto"+idproducto).val(1);
            $('.classTotal').html(parseFloat(objecto.precioVenta)*1);
            anadirProducto(1,parseFloat(objecto.precioVenta),objecto.id,objecto.nombre,objecto.img);
        }else{
            $('.classTotal').html(parseFloat(objecto.precioVenta)*cantidad);
            anadirProducto(parseInt(cantidad),parseFloat(objecto.precioVenta),objecto.id,objecto.nombre,objecto.img);
        }
        cargarProductosSeleccionado();
        
    }
    
    </script>

@stop