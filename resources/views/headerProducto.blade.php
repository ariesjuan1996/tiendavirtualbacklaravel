<section id="header-search">
    <div class="container flex space-between">
        
        <div>
            <!--<a href="index.html" class="pointer back">&LeftArrow;</a>-->
            <a href="/" class="pointer back"><i class="fa fa-arrow-left" aria-hidden="true"></i></a>
        </div>
        <input type="checkbox" id="check">
        <label for="check" class="checkbtn">
            <i class="fa fa-bars pointer" id="btn"></i>
            <i class="fa fa-close pointer" id="cancel"></i>
        </label>
        <nav id="main-nav">
            <div class="movil-nav">
                <h3 class="title-nav">Frozen Market</h3>
                <ul id="itemscategorias">
                </ul>
            </div>
        </nav>

        <div id="user-box" class="user">
            <div  class="user-form" style="cursor: pointer;">
                <span class="fa fa-user-circle" id="domUsuarioLogueado"></span>
            </div>
        </div>

        <div class="search-box">
            <input type="text" placeholder="Buscar" name="" class="search-txt">
            <a class="search-btn">
                <span class="fa fa-search"></span>
            </a>
        </div>

        <div id="car-box" class="shopping">
            <span class="monto price priceTotal">0.00</span>
            <a class="fa fa-shopping-cart cart">
                <span class="num numTotal">0</span>
            </a>
        </div>

    </div>
</section>

@include('header/formSessionOrRegistro')

@include('header/carrito')
