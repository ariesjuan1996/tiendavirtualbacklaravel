@extends('productosIndex')
@section('headerProducto')
    @include('headerProducto')
@stop

@section('containerProducto')
<section id="finalizar" style="margin-top: 40px !important;">
    <div class="container">
        <div class="row space-between">
            <div id="row-one-finalizar" class="row-one-finalizar">
                <div class="box-datos cssSession">
                    <div class="header-box row">
                        <h1 class="title-box"><b>1</b>Datos Personales</h1>
                        <a class="ancla"><span class="fa fa-pencil"></span> Modificar</a>
                    </div>
                    <div id="boxone-desl" class="box-header">
                        <div class="enlaces-op">
                            <ul>
                                <li><a class="op-one linkactivar" >Pedir como invitado</a></li>
                                <li ><span class="separador">|</span></li>
                                <li><a class="linkactivar activebtn cssSession">Iniciar Sesión</a></li>
                            </ul>
                        </div>
                        <form action="" class="formu-datos" id="iniciarSession"  >
                            <div class="col-input classDireccion">
                                <label for="" class="label-field" >Dirección de Correo Electrónico</label>
                                <input  class="input-box" type="email"  name="correo" id="correo" 
                                required autocomplete="off">
                                <label for="" class="comentario"></label>
                            </div>
                            <div class="col-input classContrasenia">
                                <label for="" class="label-field" >Contraseña</label>
                                <input  class="input-box" type="password" name="contrasenia" 
                                id="rcontrasenia" required autocomplete="off">
                                <label for="" class="comentario"></label>
                            </div>
                            <div class="pie-boxone">
                                <div class="enlaces-op">
                                    <a class="op" href="">Crear una cuenta<i>(opcional)</i></a>
                                    <p class="op-text" > ¡Y ahorre tiempo en su próximo pedido!</p>
                                </div>
                                <div class="pass">
    
                                </div>
                                <div class="uno">
                                    <div class="row">
                                        <input type="checkbox" name="recibirNotificaciones" id="recibirNotificaciones">
                                        <span>Recibir Notificaciones</span>
                                    </div>
                                    <div class="row">
                                        <input type="checkbox" name="aceptarCondiciones" id="aceptarCondiciones">
                                        <span>Acepto los términos y condiciones así como la política de privacidad.</span>
    
                                    </div>
                                </div>
                            </div>
                            <div class="col-input box-btn">
                                <input type="submit" class="btn-enviar" value="Continuar "/>
                                
                            </div>
                        </form>

                    </div>
                </div>
                <div class="box-datos">
                    <div class="header-box row">
                        <h1 class="title-box"><b>2</b>Direcciones</h1>
                        <a class="ancla"><span class="fa fa-pencil"></span> Modificar</a>
                    </div>
                    <div id="boxone-desl" class="box-header">
                        <form>
                            <div class="col-input classDireccion">
                                <label for="" class="label-field" >Dirección de Correo Electrónico</label>
                                <input  class="input-box emailRegistro" type="email"  name="correo" 
                                id="emailRegistro" required maxlength="250" autocomplete="off">
                                <label for="" class="comentario"></label>
                            </div>
                            <div class="col-input classContrasenia">
                                <label for="" class="label-field" >Contraseña</label>
                                <input  class="input-box contraseniaRegistro" type="password" 
                                name="contrasenia" id="contraseniaRegistro" required autocomplete="off">
                                <label for="" class="comentario"></label>
                            </div>   
                            <div class="col-input">
                                <label for="" class="label-field" >Nombres</label>
                                <input  class="input-box" type="text" name="nombres" 
                                id="nombres" required maxlength="250" autocomplete="off">
                                <label for="" class="comentario"></label>
                            </div>
                            <div class="col-input">
                                <label for="" class="label-field" >Apellidos</label>
                                <input  class="input-box" type="text" 
                                name="apellidos" id="apellidos" required maxlength="250" autocomplete="off">
                                <label for="" class="comentario"></label>
                            </div>
                            <div class="col-input">
                                <label for="" class="label-field" >Celular</label>
                                <input  class="input-box textnumber" type="text" maxlength="9" 
                                name="celular" id="celular" required autocomplete="off">
                                <label for="" class="comentario"></label>
                            </div>
                            <div class="col-input">
                                <label for="" class="label-field" >Dirección</label>
                                <input  class="input-box" type="text" name="direccion" 
                                id="direccion" required  maxlength="250" autocomplete="off" />
                                <label for="" class="comentario"></label>
                            </div>
                            <div class="col-input">
                                <label for="" class="label-field" >Referencia</label>
                                <input  class="input-box" type="text" name="referencia" 
                                id="referencia" required maxlength="500" autocomplete="off" />
                                <label for="" class="comentario">Opcional</label>
                            </div>
                            <div class="col-input">
                                <label for="" class="label-field"  >Departamento</label>
                                <select name="departamento" id="departamento">
                                    <option  value="" >seleccione departamento</option>
                                </select>   
                            </div> 
                            <div class="col-input">
                                <label for="" class="label-field"  >Provincia</label>
                                <select name="provincias" id="provincias">
                                    <option  value="" >seleccione provincia</option>
                                </select>   
                            </div> 
                            <div class="col-input">
                                <label for="" class="label-field" >Distrito</label>
                                <select name="distrito" id="distritos">
                                    <option  value="" >seleccione distrito</option>
                                </select> 
                            </div>
                            <div class="col-input">
                                <label for="" class="label-field" >Para la emisión de la boleta o factura</label>
                                <div class="box-radios">
                                    <label class="input-radio" for=""><input type="radio" name="tipoDocumento" class="tipoDocumento" value="D">DNI</label>
                                    <label class="input-radio" for=""><input type="radio" name="tipoDocumento" class="tipoDocumento" value="R" >RUC</label>
                                </div>
                                
                            </div>
                            <div class="col-input">
                                <label for="" class="label-field" id="nombreTipoDocumento" >Número de documento</label>
                                <input  class="input-box textnumberdocumento" type="text" name="dni" id="dni" required>
                                <label for="" class="comentario"></label>
                            </div>
                            <div class="col-input">
                                <label for="" class="label-field" >Empresa</label>
                                <input  class="input-box" type="text" name="empresa" 
                                id="empresa"  maxlength="200" required>
                            </div>
                        </form>
                        <div class="col-input box-btn">
                            <button class="btn-enviar btn-actualizarCliente">Continuar</button>
                        </div>
                    </div>
                </div>
                <div class="box-datos">
                    <div class="header-box row">
                        <h1 class="title-box"><b>3</b>Método De Envío</h1>
                        <a class="ancla"><span class="fa fa-pencil"></span> Modificar</a>
                    </div>
                    <div id="boxthree-desl" class="box-header">
                       <div class="container">
                            <div class="delivery">
                                <input type="radio" name="" id="" checked>
                                <div class="rwo-dev">
                                    <div class="carrier-name ">
                                        <img src="images/delivery.jpg">
                                        <span>Frozen Market Delivery</span>
                                    </div>
                                    <div class="carrier-delay">
                                        <span class="fechaEntrega"></span>
                                    </div>
                                    <div class="carrier-price">
                                        <span>Gratis</span>
                                    </div>
                                </div>
                            </div>
                            <div class="order-options col">
                                <label for="">Si desea dejarnos un comentario acerca de su pedido, por favor, escríbalo a continuación.</label>
                                <textarea name="comentario" id="comentario" cols="30" rows="10"></textarea>
                            </div>
                       </div>
                    </div>
                </div>

                <div class="box-datos">
                    <div class="header-box row">
                        <h1 class="title-box"><b>4</b>Pago</h1>
                        <a class="ancla"><span class="fa fa-pencil"></span> Modificar</a>
                    </div>
                    <div id="boxfour-desl" class="box-header">
                        <div class="col">
                            <label for="" class="item-box">
                                <input type="radio" id="metodoPago" value="visa">Visanet Perú <img src="images/visa.jpg" alt="">
                            </label>
                            <label for="" class="item-box">
                                <input type="checkbox" id="aceptarTerminos">
                                Estoy de acuerdo con los términos y condiciones.
                            </label>

                            <div class="col-input box-btn">
                                <button class=" btn-enviar sendEnvio">Pagar</button>
                            </div>
                        </div>
                        
                    </div>
                </div>

            </div>
            <div class="row-two-finalizar">
                <div id="resumen-fin"class="resumen">
                    <div class="resumen-total col">
                        <div class="line-products row space-between">
                            <p><span class="num-products cantidaProductos" id="cantidaProductos">0</span>artículos</p>
                        </div>
                        <div class="line-products col space-between">
                            <a class="mostrar-det" href="/carrito">Mostrar detalles</a>
                            <div  class="list-prod">
                                <ul id ="listProductos">


                                </ul>
                            </div>
                        </div>
                        <div class="line-products row space-between">
                            <p>Subtotal</p>
                            <p class="neg price" id="subTotalFinalizar">0.00</p>
                        </div>
                        <div class="line-products row space-between">
                            <p>Transporte</p>
                            <p class="neg ">Gratis</p>
                        </div>
                    </div>
                    <div class="card-resumen">
                        <div class="body-card-res row space-between">
                            <p class="neg" >Total</p>
                            <p class="neg price" id="totalFinalizar">0.00</p>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>
@stop

@section('script')
<script src="js/jquery.js"></script>
<script src="{{ URL::asset('js/query/api.js') }}"></script>
<script src="js/app.js"></script>
<script src="{{ URL::asset('js/query/seguridad.js') }}"></script>
<script src="{{ URL::asset('js/query/carrito.js') }}"></script>
<script>

    window.addEventListener("load", function(){
        cargardepartamento("");
        cargarEventos();
        estaAutenticado();
        cargarDataCarritoFinalizar();
        var f = new Date();
        let mess=(f.getMonth() +1)<10 ? ("0"+(f.getMonth() +1) ) :(f.getMonth() +1) ;
        let dia=(f.getDate())<10 ? ("0"+(f.getDate() +1) ) :(f.getDate()) ;
        fecha = f.getDate() + "/" + (f.getMonth() +1) + "/" + f.getFullYear();
       //$(".fechaEntrega").html('Entrega 20-06-2020 <br>9:00 a 19:00');
       $(".fechaEntrega").html(`Entrega ${dia}-${mess}-${(f.getFullYear())} <br>`);
        //$("#nombreTipoDocumento").text(checkTipoDocumentoD==true ? "Numero de DNI" : (checkTipoDocumentoR==true ? "Numero de ruc" : "Numero de Documento") );
    });
    $(document).ready(function(){
    $('input[type=radio][name=tipoDocumento]').change(function(){
        if(this.value=='D'){
            $("#nombreTipoDocumento").text("Numero de DNI")
        }else if(this.value=='R')
        {
            $("#nombreTipoDocumento").text("Numero de RUC")
        }
    })
    });
    $(".emailRegistro").blur(function(e) {
        let correo=e.target.value;
        
        if(!correo){
            return false;
        }
        
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if(re.test(correo)){
            let correoActual=null;
            if(localStorage.getItem("dataUsuario")!=null){
                let dataUsuarioLocal= JSON.parse(localStorage.getItem("dataUsuario") );
                correoActual=dataUsuarioLocal.correo
            }
            if(correoActual!=correo){
                api("api/verificarCorreoUsuario?email="+correo,"GET",{}).then((data)=>{
                let dataApi=JSON.parse(data);
                let estado=dataApi.estado;
                if(estado){
                    e.target.value="";
                    alert(dataApi.mensaje);
                }
                console.log(dataApi);
                
            }).catch((error)=>{
                console.log(error);
            });
            }

        }else{
            alert("Format correo invalido");

        }
    });
    function iniciarSessionFinalizar(){
        let aceptaCondiciones=$("#aceptarCondiciones").prop('checked');
        if(aceptaCondiciones==false){
            alert("Se requiere que acepte condiciones.");
            return false;
        }
        var email =  $("#correo").val();
        var contrasenia = $("#rcontrasenia").val();
        console.log(email,contrasenia);
        api("api/iniciarSession","POST",    {
            "email":email,
            "contrasenia":contrasenia
        }).then((data)=>{
            let dataApi=JSON.parse(data);
            let estado=dataApi.estado;
            if(estado){
                let token=dataApi.token;
                let dataUsuario=dataApi.data;
                let dataUsuarioString=JSON.stringify(dataUsuario);
                localStorage.setItem("token",token);
                localStorage.setItem("dataUsuario",dataUsuarioString);
                cargarDataUsuario();
                estaAutenticado();
            }else{
                let mensaje=dataApi.mensaje;
                alert(mensaje);
            }
            console.log(dataApi);
        }).catch((error)=>{
            console.log(error);
          
        });


    }
    console.log(" $('#iniciarSession')", $('#iniciarSession'));

    $( "#iniciarSession" ).submit(function( event ) {
        event.preventDefault();
        iniciarSessionFinalizar();
    });
    $(".btn-actualizarCliente").on('click',(e)=>{
       // console.log("e",e);
        let nombres=document.getElementById("nombres").value;
        let apellidos=document.getElementById("apellidos").value;
        let celular=document.getElementById("celular").value;
        let direccion=document.getElementById("direccion").value;
        let referencia=document.getElementById("referencia").value;
        let correo=$(".emailRegistro").val();
    
        let contraseniaRegistro=$(".contraseniaRegistro").val();
        let tipoDocumento=$("input.tipoDocumento[type=radio]:checked").val();
       
       
        let distritoSelect=$("#distritos option[selected]")[0];
        let documento=document.getElementById("dni").value;
        let empresa=document.getElementById("empresa").value;
        if($("#distritos").val()==""){
            alert("seleccione distrito.");
            return false;
        }
        if(nombres=="" || nombres==null){
            alert("Los nombres es requerido.");
            return false;
        }
        if(apellidos=="" || apellidos==null){
            alert("Los apellidos es requerido.");
            return false;
        }
        if(celular=="" || celular==null){
            alert("El celular es requerido.");
            return false;
        }
        if(direccion=="" || direccion==null){
            alert("La direcciòn es requerido.");
            return false;
        }
        if(documento=="" || documento==null){
            alert("El documento es requerido.");
            return false;
        }
        if(tipoDocumento=="" || tipoDocumento==null){
            alert("El tipo de documento es requerido.");
            return false;
        }
        if(empresa=="" || empresa==null){
            alert("Se requiere de empresa.");
            return false;
        }
        let idDistrito=$("#distritos").val();
        let token=localStorage.getItem("token");
        if(token!=null){
            api("api/actualizarCliente","POST",{
                "nombre":nombres,
                "email":correo,
                "tipoDocumento":tipoDocumento,
                "tipoCliente":"REGISTRADO",
                "numeroDocumento":documento,
                "distrito":idDistrito,
                "numeroCel":celular,
                "nombreEmpresa":empresa,
                "referencia":referencia,
                "direccion":direccion,
                "apellidos":apellidos,
                "estado":true
            })
            .then((data)=>{
                let datosReponse=JSON.parse(data);
                if(datosReponse.estado){
                    let dataLocalUsuario=JSON.parse(localStorage.getItem("dataUsuario"));
                    localStorage.setItem("dataUsuario",JSON.stringify({
                    "tipoUsuario": "CLIENTE",
                    "idCliente": dataLocalUsuario.idCliente,
                    "nombreCompletos": nombres,
                    "apellidos": apellidos,
                    "idUsuario":  dataLocalUsuario.idUsuario,
                    "correo": correo
                }));
                }
                cargarDataUsuario();
                estaAutenticado();
                alert(datosReponse.mensaje);
            }).catch((error)=>{
                console.log("error",error);
            }); 
        }else{
            let distrito=$("#distritos").val();
            let dataRequest={
                "nombre":nombres,
                "email":correo,
                "tipoDocumento":tipoDocumento,
                "tipoCliente":"REGISTRADO",
                "numeroDocumento":documento,
                "distrito":distrito,
                "numeroCel":celular,
                "nombreEmpresa":empresa,
                "referencia":referencia,
                "direccion":direccion,
                "apellidos":apellidos,
                "constrasenia":contraseniaRegistro
            };
            console.log("dataRequest",dataRequest);
            api("api/registraCliente","POST",dataRequest).then((data)=>{
                let datosReponse=JSON.parse(data);
                if(datosReponse.estado){
                    let dataCliente=datosReponse.data;
                    let token=datosReponse.token;
                    localStorage.setItem("token",token);
                    localStorage.setItem("dataUsuario",JSON.stringify(dataCliente));
                    cargarDataUsuario();
                    estaAutenticado();
                    alert("El usuario fue creado con exito");

                }else{
                    alert(datosReponse.mensaje);
                }
               
            }).catch((error)=>{
                alert("Hubo un error en la peticiòn");
                console.log("error",error);
            }); 

        }
 
    });
    $( "#departamento" ).change(function(e) {
        cargarProvincia("",e.target.value);
    });
    $( "#provincias" ).change(function(e) {
        cargarDistrito("",e.target.value);
    });
    function cargarDistrito(id,provincia){
        api(`api/cargarDistritos?idProvincia=${provincia}&id=${id}`,"GET",{})
        .then((data)=>{
            let dataApi=JSON.parse(data);
            let dataDistritos=dataApi.data ;

            let domdistrito=document.getElementById("distritos");
            let htmldistrito=null;
            if(id==""){
                htmldistrito=`<option value="" selected>Seleccione distrito</option>`;
            
            }else{
                htmldistrito=`<option  value="" >Seleccione distrito</option>`;
            }
            
            for (let index = 0; index < dataDistritos.length; index++) {
                const element = dataDistritos[index];
                let stringObject=JSON.stringify(element);
                if(id==""){
                    htmldistrito= htmldistrito+`<option  value="${element.id}" data-object=${JSON.stringify(element)}>${element.name}</option>`;
                }else{
                    if(index==0){
                        htmldistrito= htmldistrito+`<option selected  value="${element.id}" data-object=${JSON.stringify(element)}>${element.name}</option>`;
                    }else{
                        htmldistrito= htmldistrito+`<option   value="${element.id}" data-object=${JSON.stringify(element)}>${element.name}</option>`;

                    }
                 
                }
                
            }
            domdistrito.innerHTML="";
            domdistrito.innerHTML=htmldistrito;
            
        }).catch((error)=>{
            console.log(error);
        });  
    }

    function cargarProvincia(id,departamento){
        api(`api/cargarProvincia?idDepartamento=${departamento}&id=${id}`,"GET",{})
        .then((data)=>{
            let dataApi=JSON.parse(data);
            let dataProvincias=dataApi.data ;
        

            let domprovincia=document.getElementById("provincias");
            let htmlProvincias=null;
            if(id==""){
                 htmlProvincias=`<option value="" selected>Seleccione provincia</option>`;
            
            }else{
                htmlProvincias=`<option value="" >Seleccione provincia</option>`;
            }
            for (let index = 0; index < dataProvincias.length; index++) {
                const element = dataProvincias[index];

                if(id==""){
                    htmlProvincias= htmlProvincias+`<option  value="${element.id}" data-object=${JSON.stringify(element)}>${element.name}</option>`;
                }else{
                    if(index==0){
                      htmlProvincias= htmlProvincias+`<option selected value="${element.id}" data-object=${JSON.stringify(element)}>${element.name}</option>`;
                    }else{
                      htmlProvincias= htmlProvincias+`<option  value="${element.id}" data-object=${JSON.stringify(element)}>${element.name}</option>`;

                    }
                }
               
                        
                
            }
            domprovincia.innerHTML="";
            domprovincia.innerHTML=htmlProvincias;

            
        }).catch((error)=>{
            console.log(error);
        });  
    }
    function cargardepartamento(id){
        api(`api/cargarDepartamento?id=${id}`,"GET",{})
        .then((data)=>{
            let dataApi=JSON.parse(data);
            let dataDepartamento=dataApi.data;
            let domdepartamento=document.getElementById("departamento");
            let htmlDepartamento=null;
            if(id==""){
                htmlDepartamento=htmlDepartamento+`<option value="" selected>seleccione departamento</option>`;
            }else{
                htmlDepartamento=htmlDepartamento+`<option value="">seleccione departamento</option>`;
            }
            dataDepartamento.forEach((element,index) => {
                if(id==""){
                    htmlDepartamento=htmlDepartamento+`<option value="${element.id}" data-object=${JSON.stringify(element)}>${element.name}</option>`;

                }else{
                    if(index==0){

                      htmlDepartamento=htmlDepartamento+`<option value="${element.id}" data-object=${JSON.stringify(element)} selected>${element.name}</option>`;
                    }else{
                      htmlDepartamento=htmlDepartamento+`<option value="${element.id}" data-object=${JSON.stringify(element)} >${element.name}</option>`;

                    }
                }
            });
            domdepartamento.innerHTML="";
            domdepartamento.innerHTML=htmlDepartamento;
            
        }).catch((error)=>{
            console.log(error);
        });  
    }
    function estaAutenticado(){
      //cargarDataUsuario();
      let token=localStorage.getItem("token");
      if(token!=null){

        api("api/mostrarCliente","GET",{})
        .then((data)=>{
            let dataApi=JSON.parse(data);
            let estado=dataApi.estado;
            
            if(estado){
            let dataParse=dataApi.data;
           
            if(dataParse.tipoUsuario=="CLIENTE"){

                let idUsuario=dataParse.idUsuario;

                let correo=dataParse.correo;
                let estado=dataParse.estado;
                let tipoUsuario=dataParse.tipoUsuario;
                let idCliente=dataParse.idCliente;
                let nombres=dataParse.nombres;
                let apellidos=dataParse.apellidos;
                let fechaCreacion=dataParse.fechaCreacion;
                let direccion=dataParse.direccion;
                let tipoDocumento=dataParse.tipoDocumento;
                let tipoCliente=dataParse.tipoCliente;
                let numeroDocumento=dataParse.numeroDocumento;
                let distrito=dataParse.distrito;
                let numeroCel=dataParse.numeroCel;
                let nombreEmpresa=dataParse.nombreEmpresa;
                let referencia=dataParse.referencia;
               
                cargardepartamento(dataParse.departamento);
                cargarProvincia(dataParse.provincia,dataParse.departamento);
                cargarDistrito(dataParse.distrito,dataParse.provincia);
               
                $("input.tipoDocumento[type=radio][value="+tipoDocumento+"]").attr('checked', true);

                $("#nombreTipoDocumento").text(tipoDocumento=="D" ? "Numero de DNI" :(tipoDocumento=="R" ? "Numero de RUC" : "Numero de documento" ));
                $("#rapellidos").val(apellidos);
                $("#rnombres").val(nombres);
                $(".emailRegistro").val(correo);

                $("#nombres").val(nombres);
                $("#apellidos").val(apellidos);
                $("#celular").val(dataParse.numeroCel);
                $("#direccion").val(direccion);
                $("#referencia").val(referencia);
                $("#distrito").val(distrito);
                $("#dni").val(dataParse.numeroDocumento);
                $("#empresa").val(dataParse.nombreEmpresa);
                $(".classContrasenia").css("display","none");
                $(".cssSession").css("display","none");
                //$(".clsSession").css("display","none");
               // $('.sendEnvio').prop("disabled",true);
            }


            }
            
        }).catch((error)=>{
       
        });  
      }
       
    }
    function cargarEventos(){
        let linkactivar=document.getElementsByClassName("linkactivar");
        for (let index = 0; index < linkactivar.length; index++) {
            const element = linkactivar[index];
            element.addEventListener("click",function(e){
              document.getElementsByClassName("activebtn")[0].classList.remove("activebtn");
              //e.target.classList.addClass("activebtn");
              var activos = $(e.target);
              activos.addClass('activebtn');  
             // console.log(" e.target", e.target.className);
              //e.target.addClass("activebtn");
             // $(this).addClass('ie6rules');
            });            
        }

    }
    function cargarDataCarritoFinalizar(){
    let listProductos=localStorage.getItem("productoList");
    let domCantidaProductos=document.getElementById("cantidaProductos");
 
    if(listProductos==null){
        document.getElementById("cantidaProductos").value=0;
        window.location.href = location.origin+"/productos";
    }else{
        let sumTotal=0.0;
        let productoParse=JSON.parse(listProductos);
        let cantidaProductoTotal=0;
        let htmlDom="";
        productoParse.map((value)=>{
            console.log("value",value.img);
            cantidaProductoTotal=cantidaProductoTotal+parseInt(value.cantidad);
            let img=value.img;
            let itemProductoPrecio=parseFloat(value.precioVenta);
            let cantidad=parseInt(value.cantidad);
            sumTotal=sumTotal+parseFloat(value.precioVenta)*parseInt(value.cantidad);

             htmlDom=htmlDom+`<li class="item-cart">
                                    <div class="item-product">
                                        <div class="img-item">
                                            <img src="http://localhost:8000/img/1594333837430.jpg" alt="">
                                        </div>
                                        <div class="txt-item">
                                            <p class="name"> ${value.cantidad} X ${value.nombre } </p>
                                            <div class="price-box">
                                                <p class="det-mov">Precio Unitario</p>
                                                <p class="price">${value.precioVenta}</p>
                                            </div>
                                        </div>

                                        
                                        <div class="options-item">
                                            <div class="mostrar-input" id="inputNum">
                                                <div class="nice-number">
                                                    
                                            </div>
                                  
                                        </div>
                                    </div>
                                </li>`;
        });
        if(cantidaProductoTotal==0){
            window.location.href = location.origin+"/productos";
        }
        $("#listProductos").html(htmlDom);
        //alert("cantidaProductoTotal"+cantidaProductoTotal);
        $(".cantidaProductos").text(cantidaProductoTotal);
        $(".subTotalFinalizar").text(sumTotal);
        $(".subTotalFinalizar").text(sumTotal);
        $("#totalFinalizar").text(sumTotal);
        $("#subTotalFinalizar").text(sumTotal);
        //console.log( $(".cantidaProductos").val());
    }
}
$(".tipoDocumento").on('change',function(e){
    if($(e.target).val()=="D"){
       
       $('.textnumberdocumento').val($('.textnumberdocumento').val().substring(0,8)) ;
    }else{
       $('.textnumberdocumento').val($('.textnumberdocumento').val().substring(0,8)) ;
    }
});
$('.sendEnvio').on('click',function(e){
    var listProductosLocal=localStorage.getItem("productoList");
    if(listProductosLocal!=null){
        var productoParse=JSON.parse(listProductosLocal);
        if(productoParse.length==0){
            alert("Primero seleccione algun producto.");
        }else{
            if($('#aceptarTerminos:checked').val()=="on"){
                let tipoPago=$("#metodoPago[type=radio]:checked").val();
                console.log("tipoPago : ",tipoPago);
                if(tipoPago==undefined){
                    alert("Elija metodo de pago.");
                }else{
                   let comentario=$('#comentario').val()? $('#comentario').val() : null;
                   let detalles=[];
                   for (let index = 0; index < productoParse.length; index++) {
                    detalles.push({
                        "idProducto":productoParse[index].id,
                        "cantidad":productoParse[index].cantidad
                    });
                   }
                   let seguirTransaccion={
                    "tokenPago":"asas4a5s45a4s5a4sa5s4a5s4a54s4sa5s4",
                    "estado":"PEDIDO",
                    "tipoPago":tipoPago,
                    "comentario":comentario,
                    "detallePedido":detalles
                };
                api("api/realizarPedido","POST",seguirTransaccion).then((data)=>{
                    let dataApi=JSON.parse(data);
                    if(dataApi.estado){
                      alert(dataApi.mensaje);
                      localStorage.removeItem("productoList");
                      window.location.href =window.location.origin ;
                    }else{
                      alert(dataApi.mensaje);

                    }
                }).catch((error)=>{
                    console.log(error);
                });
                }
                
            }else{
                alert("Acepte primero condiciones.");
            }
            
            //alert("Puede realizar la compra");
        }
    }else{
        alert("Primero seleccione algun producto.");
    }
        
});
$('.textnumberdocumento').keyup(function(e)
{
  if (/\D/g.test(this.value))
  {
    
    this.value = this.value.replace(/\D/g,'');
    
  }else{
    let tipoDocumento=$("input.tipoDocumento[type=radio]:checked").val();
    if(tipoDocumento=="D"){
        this.value=this.value.substring(0,8);
        //$('.textnumberdocumento').val($('.textnumberdocumento').substring(0,8)) ;
    }else{
        this.value=this.value.substring(0,11);

    }
  }
}); 
$('.textnumber').keyup(function(e)
{
    console.log("e",e);
  if (/\D/g.test(this.value))
  {
    this.value = this.value.replace(/\D/g,'');
  }
});   
</script>
@stop