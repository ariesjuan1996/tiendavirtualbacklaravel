<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no ,initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <title>Document</title>

  
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.0.0/animate.min.css" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@100;300;400;500;700;900&display=swap" rel="stylesheet">
    
    
    
    <link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
    
    <link href="{{ URL::asset('css/css.css') }}" rel="stylesheet" type="text/css" >
    
    <link href="{{ URL::asset('css/normalize.css') }}" rel="stylesheet" type="text/css" >
    <link href="{{ URL::asset('css/jquery.nice-number.css') }}" rel="stylesheet" type="text/css" >
    <link href="{{ URL::asset('css/owl.carousel.min.css') }}" rel="stylesheet" type="text/css" >
</head>

<body>
    @yield('header')
    @yield('siderbar')
    @yield('comunicado')
    @yield('categoriasImagen')
    @yield('footer')
    <!--SCRIPTS-->
    <script src="{{ URL::asset('js/jquery.js') }}"></script>
    <script src="{{ URL::asset('js/app.js') }}"></script>
    
    <script src="{{ URL::asset('js/owl.carousel.min.js') }}"></script>
    <script src="{{ URL::asset('js/jquery.nice-number.min.js') }}"></script>
    <script type="text/javascript">
        window.addEventListener("scroll", function() {
            var header = document.querySelector("header");
            header.classList.toggle("sticky", window.scrollY > 0);

            if (window.scrollY > 15 && screen.width < 1240) {
                // alert(':D');

                                $('#logo').css({
                                    position: 'relative',
                                    top: '25px',
                                    left: '0'
                                });
                                
                                $('.row-header').css({
                                    position: 'relative',
                                    top: '-25px'
                                });
                                $('.search-box').css({
                                    background: 'none',
                                    width: '90%'
                                });
                                
                                $('.search-txt').css({
                                    display: 'none'
                                });
                                $('#user-box').css({
                                    position: 'relative',
                                    left: '-20%'
                                });
                                $('#toggle-box-search').css({
                                    display: 'none'
                                });
                                $('.toggle-btn-nav').css({
                                    display: 'block',
                                    position: 'relative',
                                    left: '-8px'
                                });

            }

        })

    </script>

    <script>
        $('#slider-area').owlCarousel({
            items: 1,
            autoplayTimeout: 3000,
            responsiveClass: true,
            autoplaySpeed: 1000,
            responsive: {
                0: {
                    items: 1,
                    nav: true
                },
                600: {
                    items: 1,
                    nav: true
                },
                1000: {
                    items: 1,
                    nav: true,

                }
            }
        });

    </script>
    <script>
        $('#slider-options').owlCarousel({
            items: 1,
            autoplayTimeout: 3000,
            responsiveClass: true,
            autoplaySpeed: 1000,
            responsive: {
                0: {
                    items: 2,
                    nav: false
                },
                600: {
                    items: 3
                },
                1000: {
                    items: 5,
                }
            }
        });

    </script>


    <script>
        $(function() {
            $('input[type="number"]').niceNumber();
        });

    </script>
       <script src="{{ URL::asset('js/query/api.js') }}"></script>
       <script src="{{ URL::asset('js/query/productosindex.js') }}"></script>
       <script src="{{ URL::asset('js/query/seguridad.js') }}"></script>
       <script src="{{ URL::asset('js/query/carrito.js') }}"></script>
</body>

</html>
