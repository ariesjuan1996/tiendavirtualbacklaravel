<header id="header">
    <div id="one" class="one container">
        <div id="logo"><img src="images/logo_header.jpg" alt="Homepage"></div>


        <div class="row-header space-between">
            <input type="checkbox" id="check">

            <label for="check" class="checkbtn">
                <i class="fa fa-bars" id="btn"></i>
                <i class="fa fa-close" id="cancel"></i>
            </label>
            <nav id="main-nav">
                <div class="movil-nav">
                    <h3 class="title-nav">Frozen Market</h3>
                    <ul id="itemscategorias">
                    </ul>
                </div>
            </nav>
            <div class="row-rev">
                <div class="search-box">
                    <div class="toggle-btn-nav">
                        <a class="search-btn">
                            <span class="fa fa-search"></span>
                        </a>
                    </div>
                    <div class="toggle-box-search" id="toggle-box-search">
                        <input type="text" placeholder="Buscar" name="" class="search-txt">
                        <a class="search-btn">
                            <span class="fa fa-search"></span>
                        </a>
                    </div>
                </div>
                <div id="user-box" class="user">
                    <a href="" class="user-form">
                        <span class="fa fa-user-circle" id="domUsuarioLogueado"></span>
                    </a>
                </div>
            </div>

            <div id="car-box" class="shopping">
                <span class="monto price priceTotal">0.00</span>
                <a class="fa fa-shopping-cart cart">
                    <span class="num numTotal">0</span>
                </a>
            </div>

        </div>
    </div>
</header>
@include('header/formSessionOrRegistro')
@include('header/carrito')